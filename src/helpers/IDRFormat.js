export default function formatRupiah(angka) {
  let counter = 1;
  let rupiah = ''
  let dana = ''
  if (typeof angka === 'string') {
    dana = angka.split('').reverse()
  } else {
    dana = angka.toString().split('').reverse()
  }

  for (let i = 0; i < dana.length; i++) {
    rupiah += dana[i]
    if (counter % 3 === 0 && i !== dana.length - 1) {
      rupiah += "."
    }

    counter++
  }

  rupiah = rupiah.split('').reverse().join('')

  return rupiah;
}
