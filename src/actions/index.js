import {
  FETCH_MERCHANT_DATA,
  GET_CURRENT_LOCATION, 
  GET_DATA_FROM_MAPS,
  FETCH_PRODUCT_DATA,
  MERCHANT_HAS_BEEN_CHOOSEN,
  MERCHANT_DATA_CHOOSEN,
  AFTER_CHOOSE_FOOD,
  DATA_FROM_CHOOSE_FOOD,
  SUBTOTAL_PRICE,
  FETCH_MERCHANT_CATEGORY_SNACK,
  FETCH_MERCHANT_CATEGORY_WESTERN,
  FETCH_MERCHANT_CATEGORY_MAKANAN_TRADISIONAL,
  FETCH_MERCHANT_CATEGORY_FAST_FOOD,
  DATA_LONG,
  FETCH_MERCHANT_RECOMMENDED,
  PLACE_ORDER_DATA,
  READY_FOR_ORDER_DATA,
  SEARCH_HELPER_PAGE} from './types';
import axios from 'axios';
import { AsyncStorage, Alert, NetInfo } from 'react-native'
import { bindActionCreators } from 'redux';
const apiUrl= 'http://ec2-13-229-152-244.ap-southeast-1.compute.amazonaws.com:9000/api/';
// ec2-13-229-152-244.ap-southeast-1.compute.amazonaws.com:9000/api/sys/ping
export const fetchDataMerchant=( data ) => {
  return {
    type: FETCH_MERCHANT_DATA,
    data
  }
}

export const readyForOrderData=( data ) => {
  return {
    type: READY_FOR_ORDER_DATA,
    data
  }
}

export const fetchDataMerchantCategorySnack=(data)=>{
  return {
    type:FETCH_MERCHANT_CATEGORY_SNACK,
    data
  }
}

export const fetchDataMerchantCategoryWestern=(data)=>{
  return {
    type:FETCH_MERCHANT_CATEGORY_WESTERN,
    data
  }
}

export const fetchDataMerchantCategoryMakananTradisional=(data)=>{
  return {
    type:FETCH_MERCHANT_CATEGORY_MAKANAN_TRADISIONAL,
    data
  }
}
export const fetchDataMerchantCategoryFastFood=(data)=>{
  return {
    type:FETCH_MERCHANT_CATEGORY_FAST_FOOD,
    data
  }
}
export const fetchDataMerchantRecommended=(data)=>{
  return {
    type:FETCH_MERCHANT_RECOMMENDED,
    data
  }
}

export const sendDataFromMapsHome= (data) => {
  return {
    type : GET_DATA_FROM_MAPS,
    data
  }
}

export const fetchDataProductFromMerchant =(data) => {
  return {
    type: FETCH_PRODUCT_DATA,
    data
  }
}

export const chooseMerchant = (data)=> {
  return {
    type: MERCHANT_HAS_BEEN_CHOOSEN,
    data
  }
}

export const merchantDataChoosen=(data)=> {
  
  return {
    type: MERCHANT_DATA_CHOOSEN,
    data
  }
}

export const afterChooseFood=(data)=> {
  return {
    type: AFTER_CHOOSE_FOOD,
    data
  }
}

export const fetchDataFromChooseFood=(data)=> {
  return {
    type: DATA_FROM_CHOOSE_FOOD,
    data
  }
}

export const dataSubtotalPrice=(data)=> {
  return {
    type: SUBTOTAL_PRICE,
    data
  }
}
export const dataLong=(data)=> {
  return {
    type: DATA_LONG,
    data
  }
}

export const postPlaceOrderData=(data)=> {
  return {
    type: PLACE_ORDER_DATA,
    data
  }
}
export const searchHelperData=(data)=> {
  // alert(JSON.stringify(data))
  return {
    type: SEARCH_HELPER_PAGE,
    data
  }
}

export const dataAfterChooseFood=(value)=> {
  return(dispatch)=> {
    return dispatch(afterChooseFood(value))
  }
}

export const subtotalPrice = (value)=> {
  return (dispatch)=>{
    return dispatch(dataSubtotalPrice(value))
  }
}

export const tambahBelanja=(value)=> {
  return (dispatch)=> {
    return dispatch(tambahBelanjaData(value))
  }
}

export const dataFromMaps =(value) => {
  return (dispatch)=> {
    return dispatch(sendDataFromMapsHome(value))
  }
}

export const merchantChoosen =(value)=>{
return (dispatch)=> {
    return dispatch(merchantDataChoosen(value))
  }
}
export const fetchProductFromMerchant = (value)=> {
  return ( dispatch ) => {
    return axios.get(`${apiUrl}merchant/retrieveProduct?merchantId=${value}`)
    .then(({data}) => {
      let object = []
      for (let i = 0; i < data.result.length; i++) {
        data.result[i].qwt = 0
        object.push(data.result[i])
      }
      return dispatch(fetchDataProductFromMerchant(object))
    })
    .catch((error) => {
      alert(error + 'Error fetch Product Merchant')
    })
  }
}
export const postPlaceOrder=(value)=>{
  // alert(JSON.stringify(value))
  var dataLocalMerchant = null
  var kosong1=[]
  return(dispatch)=>{
    
    AsyncStorage.getItem('LONG_LAT')
    .then((dataLongLat)=>{
      let dataKoordinat = JSON.parse(dataLongLat)
      // alert(dataKoordinat)
      let dataValue =[]
      let pricePost =0;
      let quantityPost=0
      for (let i = 0; i < value.length; i++) {
        let hitungPricePost =Number(value[i].qwt)*Number(value[i].price)
        pricePost+=hitungPricePost
        let hitungQtyPost =Number(value[i].qwt)
        quantityPost+=hitungQtyPost
        let object = {
          productId: value[i].id,
          name:value[i].name,
          quantity:value[i].qwt
        }
        dataValue.push(object)
      }
      AsyncStorage.getItem('MERCHANT_ALL')
      .then((data)=>{
        let data21=[]
        dataLocalMerchant = JSON.parse(data)
        for(var i=0;i<value.length;i++){
          let dataHasilFind = dataLocalMerchant.find(obj => {
            return obj.id === value[i].foodMerchantId
          })
          let ok={
            long:dataHasilFind.long,
            lat:dataHasilFind.lat
          }
          axios.post(`${apiUrl}order/`,{
            title: 'Pembelian',
            userId: 'string',
            merchantLong:ok.long,
            merchantLat: ok.lat,
            price: pricePost,
            quantity: quantityPost,
            products: dataValue,
            long:dataKoordinat.long,
            lat: dataKoordinat.lat,
          }
          ,{ headers: {'Content-Type': "application/json;charset=UTF-8",}}
          )
          // return dispatch(searchHelperData(dataLocalMerchant[dataHasilCompare]))
          .then((datahasil)=>{
          // alert(JSON.stringify(dataLocalMerchant[dataHasilCompare]))
            // alert(JSON.stringify(dataHasilFind))
            return (
              dispatch(postPlaceOrderData(datahasil.data)),
              dispatch(searchHelperData(dataHasilFind))
              )
          })
          .catch((err)=>{
            alert(err)
          })
        }
      })
      .catch((err)=>{
        alert('disiniJSON.stringify(err)')
      })
        
    })
    .catch((err)=>{
      alert('JSON.stringify(err)eaeaea')
    })
  }
}

export const readyForOrder =(value)=> {
  return(dispatch)=>{
    return dispatch(readyForOrderData(value))
  }
}

export const fetchFromChooseFood =(value)=> {
  return(dispatch)=>{
    return dispatch(fetchDataFromChooseFood(value))
  }
}

export const fetchAgainProductFromMerchant = (value)=> {
  return ( dispatch ) => {
    return dispatch(fetchDataProductFromMerchant(value))
  }
}


export const fetchProductFromMerchantDup = (value)=> {
  return ( dispatch ) => {
    let kosongKanVariabel = ''
    return dispatch(fetchDataProductFromMerchant(kosongKanVariabel))
    
  }
}
export const fetchMerchant =(lat,long) => {
  let snackFood=[]
  let masakanTradisional =[]
  let fastFood =[]
  let western=[]
  let recommendedMerchant=[]
  return ( dispatch ) => {
    AsyncStorage.getItem('LONG_LAT')
    .then((dataLongLat)=>{
      let dataObjekLongLat =JSON.parse(dataLongLat)
      return axios.get(`${apiUrl}merchant/retrieveMerchant?long=${dataObjekLongLat.long}&lat=${dataObjekLongLat.lat}&radius=500`)
      .then(({data})=>{
        AsyncStorage.setItem('MERCHANT_ALL',JSON.stringify(data.result))
      if (!data) {
      } else {
        for (let i = 0; i < data.result.length; i++) {
          if (data.result[i].tag) {
            if (data.result[i].tag[0] === 'recommended') {
              recommendedMerchant.push(data.result[i])
            }
          } 
          if(data.result[i].category=== 'snack'){
            snackFood.push(data.result[i])
          } 
          if (data.result[i].category=== 'makanantradisional') {
            masakanTradisional.push(data.result[i])
          } 
          if (data.result[i].category=== 'fastfood') {
            fastFood.push(data.result[i])
          } 
          if (data.result[i].category=== 'western') {
            western.push(data.result[i])
          }
        }
      }
      return (
          dispatch(fetchDataMerchantRecommended(recommendedMerchant)),
          dispatch(fetchDataMerchantCategoryWestern(western)),
          dispatch(fetchDataMerchantCategoryMakananTradisional(masakanTradisional)),
          dispatch(fetchDataMerchantCategoryFastFood(fastFood)),
          dispatch(fetchDataMerchantCategorySnack(snackFood)), 
          dispatch(fetchDataMerchant(data))
          )
      })
      
    .catch((error) => {
      alert(error+`masuk error`)
    })
  })
  .catch((err)=>{
    alert('masukerror')
  })

  }
}



export const grabProductInput = (data) => {
  return {
    type: 'DATA_PRODUCT_INPUT',
    data
  }
}

export const productInput = (value) => {
  return(dispatch) => {
    return dispatch(grabProductInput(data))    
  }
}