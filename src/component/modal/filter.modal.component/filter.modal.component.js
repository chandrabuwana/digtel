import React, { Component } from 'react';
import { Platform,
  View,
  Text,
  Modal,
  StyleSheet,
  Dimensions,
  Slider,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import {connect} from 'react-redux'
import Images from '@assets/images';
const { width, height } = Dimensions.get("window");

class FilterModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      range:0
    }
    
  }
  getVal(val){
    
    } 
    goBackToBeranda(){
      this.props.navigation.navigate('Beranda')
    }
  render() { 
    return ( 
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={{flexDirection:"row",flex:1,}}>
            <View style={{width:40}}>
              <View style={{flex:1,alignItems:'center',justifyContent:'center',}}>                  
                <TouchableOpacity onPress={()=>{this.goBackToBeranda()}}>
                <Image 
                  source={Images.arrowBackIcon}
                  style={{width:width/22,height:width/22,alignSelf:'center'}}
                  // color={tintColor}
                />
              </TouchableOpacity>
            </View>
          </View>
            <View style={{flex:2,alignSelf:'center',justifyContent:'flex-start',}}>
              <Text style={{color:'#454545',fontSize:width/25,fontWeight:'bold',left:'5%'}}>Beli Makanan</Text>
            </View>              
          </View>   
        </View>
        <ScrollView>

        <View style={styles.content}>
        <View style={{borderBottomColor:'gray',borderBottomWidth:.5,}}>
          <View style={{height:height/7,justifyContent:'center',width:width/1.05,right:'4%',left:'4%'}}>
            <View style={{flexDirection:'row'}}>
              <View style={{flex:1}}>
                <Text>Jarak dari Lokasimu</Text>
              </View>
              <View style={{flex:1}}>
                <Text style={styles.rangeText}>{this.state.range}km</Text>
              </View>
            </View>
          <Slider
            style={{ width: 300 }}
            step={10}
            minimumValue={10}
            maximumValue={500}
            value={this.state.range}
            onValueChange={val => this.setState({ range: val })}
            onSlidingComplete={ val => this.getVal(val)}
            />
          </View>

        </View>
        {/* ================= Helpers filter ================ */}
        <View style={{borderBottomColor:'gray',borderBottomWidth:.5,}}>
          <View style={{height:height/7,justifyContent:'center',width:width/1.05,right:'4%',left:'4%'}}>
            <Text>Helpers</Text>
            <View style={{flexDirection:'row',}}>
              {/* <View style={{flex:1}}> */}
                <TouchableOpacity>
                  <View style={styles.inerViewButton}>
                    <View style={styles.subInerViewButton}>
                        <Text style = {styles.textButtonEdit}>Verified</Text>
                    </View>
                  </View>
                </TouchableOpacity>

              {/* </View> */}
              {/* <View style={{flex:1}}> */}
                <TouchableOpacity>
                  <View style={styles.inerViewButton}>
                    <View style={styles.subInerViewButton}>
                        <Text style = {styles.textButtonEdit}>All</Text>
                    </View>
                  </View>
                </TouchableOpacity>

              {/* </View> */}
            </View>
          </View>
        </View>
        {/*====================  helpers Rating====================== */}
        <View  style={{borderBottomColor:'gray',borderBottomWidth:.5,}}>
          <View style={{height:height/7,justifyContent:'center',width:width/1.05,right:'4%',left:'4%'}}>

          <Text>Helpers Rating</Text>
          <View style={{flexDirection:'row',}}>
            
              <TouchableOpacity>
                <View style={styles.inerViewButton}>
                  <View style={styles.subInerViewButton}>
                      <Text style = {styles.textButtonEdit}>Bronze</Text>
                  </View>
                </View>
              </TouchableOpacity>
            
            
              <TouchableOpacity>
                <View style={styles.inerViewButton}>
                  <View style={styles.subInerViewButton}>
                      <Text style = {styles.textButtonEdit}>Gold</Text>
                  </View>
                </View>
              </TouchableOpacity>
            
            
              <TouchableOpacity>
                <View style={styles.inerViewButton}>
                  <View style={styles.subInerViewButton}>
                      <Text style = {styles.textButtonEdit}>Silver</Text>
                  </View>
                </View>
              </TouchableOpacity>
            
            
              <TouchableOpacity>
                <View style={styles.inerViewButton}>
                  <View style={styles.subInerViewButton}>
                      <Text style = {styles.textButtonEdit}>All</Text>
                  </View>
                </View>
              </TouchableOpacity>
            
            </View>
          </View>

        </View>

          {/* ============================Merchant ======================== */}
          <View style={{borderBottomColor:'gray',borderBottomWidth:.5,}}>

            <View style={{height:height/7,justifyContent:'center',width:width/1.05,right:'4%',left:'4%'}}>

              <View style={{flexDirection:'row',paddingBottom:height/50}}>
                <View style={{flex:1}}>
                  <Text>Merchant</Text>
                </View>
                <View style={{flex:1,color:'#fb4a6f'}}>
                  <Text style={{textAlign:'justify'}}>lihat semua</Text>
                </View>
              </View>
              <View style={{flexDirection:'row'}}>
                
                  <TouchableOpacity>
                    <View style={styles.inerViewButton}>
                      <View style={styles.subInerViewButton}>
                          <Text style = {styles.textButtonEdit}>Makanan</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
             
                
                  <TouchableOpacity>
                    <View style={styles.inerViewButton}>
                      <View style={styles.subInerViewButton}>
                          <Text style = {styles.textButtonEdit}>Sayur</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
           
                
                  <TouchableOpacity>
                    <View style={styles.inerViewButton}>
                      <View style={styles.subInerViewButton}>
                          <Text style = {styles.textButtonEdit}>Fast Food</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
           
                
                  <TouchableOpacity>
                    <View style={styles.inerViewButton}>
                      <View style={styles.subInerViewButton}>
                          <Text style = {styles.textButtonEdit}>All</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
              
              </View>
            </View>
          </View>

          {/* ================Merchant Rating============== */}
          <View style={{borderBottomColor:'gray',borderBottomWidth:.5,}}>

            <View style={{height:height/7,justifyContent:'center',width:width/1.05,right:'4%',left:'4%'}}>
              <View style={{flexDirection:'row'}}>
                <View style={{flex:1}}>
                  <Text>Merchant Rating</Text>
                </View>
                <View style={{flex:1}}>
                  <Text>lihat semua</Text>
                </View>
                </View>
                  <View style={{flexDirection:'row'}}>
                    <TouchableOpacity>
                      <View style={styles.inerViewButton}>
                        <View style={styles.subInerViewButton}>
                            <Text style = {styles.textButtonEdit}>Gold</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                      <View style={styles.inerViewButton}>
                        <View style={styles.subInerViewButton}>
                            <Text style = {styles.textButtonEdit}>Silver</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                      <View style={styles.inerViewButton}>
                        <View style={styles.subInerViewButton}>
                            <Text style = {styles.textButtonEdit}>Bronze</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                      <View style={styles.inerViewButton}>
                        <View style={styles.subInerViewButton}>
                          <Text style = {styles.textButtonEdit}>All</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                </View>
            </View>
          </View>
          </View>
        </ScrollView>
          <View style={{height:height/11.5,width:width,backgroundColor:'#ffffff',bottom:0,position:'absolute'}}>
            <View style={{position:'relative',height:30,backgroundColor:'#fb4a6f',width:width/1.2,alignSelf:'center',borderRadius:5,margin:10,justifyContent:'center'}}>
              <TouchableOpacity onPress={()=>{this.goToPaymentOrdered()}}>
                <Text style={{textAlign:'center',color:'#fff'}}>Terapkan Filter</Text>
              </TouchableOpacity>
            </View>
          </View>
      </View>
     );
  }
}

const styles = StyleSheet.create({

    container: {
      flex: 1,
      flexDirection:'column',
      backgroundColor:'#ffffff'
    },
    header: {
      height:height/15,
      backgroundColor:'#ffffff',
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5, 
    },
    content:{
      backgroundColor:'#ffffff',
      flex:10,
      flexDirection:'column'
    },
    footer:{
      width:width,
    flex:1,
    height:height/11.5,
    bottom:0,
    position:'absolute'
    },
    inerViewButton: {
      paddingRight:width/30,
      alignItems:'center',
      justifyContent:'center',
    },
    subInerViewButton: {
      borderRadius:5,
      width:width/5,
      padding:5,
      color:'white',
      backgroundColor:'#fb4a6f',
      alignContent:'center',
      alignItems:'center',
    },
})
const mapStateToProps = (state,) => {
  return {
   //  prop: state.prop
  }
}
const mapDispatchToProps = (dispatch,) => {
  return {
   //  dispatch1: () => {
   //    dispatch(actionCreator)
   //  }
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(FilterModal);