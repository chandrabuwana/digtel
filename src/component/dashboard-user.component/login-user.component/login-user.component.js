import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  // TextInput,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import TextInput from '../../Input/TextInput/index'
import { connect } from 'react-redux'
import Images from '@assets/images';
import Button from '../../Button/buttonTap/index'
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
type Props = {};
class LoginUser extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  componentWillMount(){
  }
  render() {
    return(
      <View style={styles.container}>
        <View style={{flex:1,justifyContent:'center'}}>
          <Image source={Images.junkFoodIcon}
          style={{width:width/2,height:width/12}}/>
        </View>

        <View style={{flex:1,alignItems:'center'}}>
          <TextInput
          placeholder={'08521231xxxx'}
          placeholderTextColor={'#4b4a4b'}
          keyboardType={'numeric'}
          />
          <Button
          style={{flex:1,}}
          ht={8.8}
          wh={1.5}
          text={'Masuk'}
          onPress={this._onPressLoginButton}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center',
    justifyContent:'center'
  },
});

const mapStateToProps = (state) => {
  return {
    // indicatorLogin: state.filterData.indicatorLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginUser)
