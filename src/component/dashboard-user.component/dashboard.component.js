import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import Button from '../Button/buttonTap/index';
import { connect } from 'react-redux'
import Images from '@assets/images';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
type Props = {};
class Dashboard extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  componentWillMount(){
  }
  _onPressLoginButton=()=>{
    this.props.navigation.navigate('LoginUser')
  }
  render() {
    return(
      <View style={styles.container}>
        <View style={{flex:1,justifyContent:'center'}}>
          <Image source={Images.junkFoodIcon}
          style={{width:width/2,height:width/12}}/>
        </View>
        <View style={{flex:1}}>
          <View style={{flexDirection:'row',paddingTop:5}}>
            <Button
            style={{flex:1,}}
            ht={8.8}
            wh={2.5}
            text={'Masuk'}
            onPress={this._onPressLoginButton}
            />
            {/* <View style={{flex:1}}></View> */}
            <Button
            style={{flex:1,justifyContent:'center'}}
            ht={8.8}
            wh={2.5}
            text={'Pendaftaran'}
            />
          </View>
          <View style={{paddingTop:30,paddingBottom:20}}>
            <Text style={{textAlign:'center'}}>atau masuk dengan</Text>
          </View>
          <View style={{flexDirection:'row',}}>
            <View style={{flex:1,}}>
              <Image source={Images.facebookIcon} style={{width:width/7,height:width/7.111,alignSelf:'center'}}/>
            </View>
            <View style={{flex:1}}>
              <Image source={Images.instagramIcon} style={{width:width/7,height:width/7,alignSelf:'center'}}/>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center',
    justifyContent:'center'
  },
});

const mapStateToProps = (state) => {
  return {
    // indicatorLogin: state.filterData.indicatorLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
