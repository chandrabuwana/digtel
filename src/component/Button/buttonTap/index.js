import React, { Component } from 'react'
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity
} from 'react-native'

const {height,width} = Dimensions.get('window')

class ButtonTap extends Component {
  render() {
    const { text, onPress, ht, wh } = this.props
    return (
      <TouchableOpacity
        style={[styles.button, {width: width / wh, height: width / ht}]}
        onPress={onPress}
      >
        <Text style={[styles.whiteText, styles.fontSize6pt]}>{text}</Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fb4a6f',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#fb4a6f',
    padding: 5,
    elevation: 2,
    margin:5
  },
  fontSize6pt: {
    fontSize: height/45
  },
  whiteText: {
    fontFamily: 'Montserrat Regular',
    color: 'white'
  },
})

export default ButtonTap