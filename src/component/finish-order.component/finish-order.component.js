import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,PanResponder
} from 'react-native';
import { connect } from 'react-redux'
import Images from '@assets/images';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
const Gambar=[
  { uri:"https://d.zmtcdn.com/data/pictures/chains/8/16774318/a54deb9e4dbb79dd7c8091b30c642077_featured_v2.png"},
  { uri:"https://d.zmtcdn.com/data/pictures/chains/8/16774318/a54deb9e4dbb79dd7c8091b30c642077_featured_v2.png"},
  { uri:"https://d.zmtcdn.com/data/pictures/chains/8/16774318/a54deb9e4dbb79dd7c8091b30c642077_featured_v2.png"},
  { uri:"https://d.zmtcdn.com/data/pictures/chains/8/16774318/a54deb9e4dbb79dd7c8091b30c642077_featured_v2.png"},
  { uri:"https://d.zmtcdn.com/data/pictures/chains/8/16774318/a54deb9e4dbb79dd7c8091b30c642077_featured_v2.png"},
  { uri:"https://d.zmtcdn.com/data/pictures/chains/8/16774318/a54deb9e4dbb79dd7c8091b30c642077_featured_v2.png"},
]
type Props = {};
class FinishOrder extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      markers: [
        {          
          title: "Pizza Ahoy",
          description: "This is the best place in Portland",
          image: Gambar[0],
          price: 20000,
          count:0,
        },
        {          
          title: "Bakwan Jagung Super",
          description: "This is the second best place in Portland",
          image: Gambar[1],
          price: 20000,
          count:0,
        },
        {          
          title: "Third Best Place",
          description: "This is the third best place in Portland",
          image: Gambar[2],
          price: 20000,
          count:0,
        },
        {          
          title: "Fourth Best Place",
          description: "This is the fourth best place in Pasdjlknaskjbnascortland",
          image: Gambar[3],
          price: 20000,
          count:0,
        },{          
          title: "Fourth Best Place",
          description: "This is the fourth best place in Portland",
          image: Gambar[4],
          price: 20000,
          count:0,
        },
      ],
    }
  }
  componentWillMount(){
    this._panResponder = PanResponder.create({
      onMoveShouldSetResponderCapture: () => true,
      onMoveShouldSetPanResponderCapture: () => true,
      onPanResponderGrant: (e, gestureState) => {
        this.fScroll.setNativeProps({ scrollEnabled: false })
      },
      onPanResponderMove: () => {

      },
      onPanResponderTerminationRequest: () => true,
      onPanResponderRelease: () => {
        this.fScroll.setNativeProps({ scrollEnabled: true })
      },
    })
    // this._panResponder = PanResponder.create({
    //   onPanResponderTerminationRequest: () => false,
    //   onStartShouldSetPanResponderCapture: () => true,
    // });
  }
  goToChoosenHelper(){
    this.props.navigation.navigate('HelperDataProfile')
  }
  goToBeranda(){
    this.props.navigation.navigate('Beranda')
  }
  render() {
    return(
      <View style={styles.container}>
        
        <View style={styles.content}>
        <View style={{flexDirection:"row",flex:1}}>
          <View style={{width:40}}>
            <View style={{flex:1,alignItems:'center',justifyContent:'center',}}>                  
              <TouchableOpacity onPress={()=>{this.goToBeranda()}}>
              <Image 
                source={Images.closeIcon}
                style={{width:width/22,height:width/22,alignSelf:'center'}}
              />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{flex:2,alignSelf:'center',justifyContent:'flex-start',}}>
            <Text style={{fontSize:width/25,fontWeight:'bold',left:'5%',color:'#454545'}}>Order Anda Telah Selesai</Text>
          </View>
          <View style={{flex:1,justifyContent:'flex-end',alignItems:'flex-end'}}>
          </View>
        </View>
      </View>
      <View style={{flex:1,marginBottom:height/11.5}}>

      <ScrollView ref={(e) => { this.fScroll = e }}>
      <View style={{
        height:height/8,
        flexDirection:'row',
        borderBottomColor:'gray',
        borderBottomWidth:.5,
        
        }}>
          <View style={{ 
            flex:1,
            flexDirection:'row',
            marginLeft:width/20,
            marginRight:width/20
            }}>
            <TouchableOpacity onPress={()=> {this.goToHelperDataProfile()}}>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <Image
              source={{uri:'https://awsimages.detik.net.id/community/media/visual/2016/11/09/3258de99-4a22-4a16-b1c2-8791eaf9c8ec.jpg?w=780&q=90'}}
              style={{width:width/5,height:width/5,borderRadius:150/2}}
              />
              </View>
            </TouchableOpacity>

            <View style={{flex:1,flexDirection:'column'}}>
              <View style={{flex:1,justifyContent:"center",alignItems:"center"}}>
              <Text style={{color:'#4a4a4a',fontWeight:'600'}}>Chelsea Islan</Text>
              </View>
              <View style={{flex:1,flexDirection:'row'}}>
                <View style={{flex:1}}>
                <Image 
                  source={Images.goldMedalIcon}
                  size={10}
                />
                </View>
                <View style={{flex:2,justifyContent:"center"}}>
                  <View style={styles.verifiedBox}>
                    <Text style={{color:'#454545'}}>Verified</Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View style={{ flex:1,flexDirection:'row'}}>
            <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
            </View>
            <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
            </View>
          </View>
        </View>
        <View style={{height:height/5,borderBottomColor:'gray',borderBottomWidth:.5}}>
          <View style={{paddingTop:10}}>
            <Text style={{textAlign:'center',color:'#4a4a4a'}}>Bagaimana pengalaman anda dengan jasa</Text>
            <Text style={{textAlign:'center',color:'#4a4a4a'}}>Chelsea Islan ?</Text>
          </View>
            <View style={{flexDirection:'row',marginTop:10}}>
              <View style={{flex:1,justifyContent:'center',alignItems:'center',}}>
              <TouchableOpacity>
              <Image 
                  source={Images.sadIcon}
                  size={10}
                  // style={{height:width/6,width:width/6}}
                />
              </TouchableOpacity>
              </View>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <TouchableOpacity>
              <Image 
                  source={Images.mehIcon}
                  size={10}
                  // style={{height:width/6,width:width/6}}
                />
              </TouchableOpacity>
              </View>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <TouchableOpacity>
              <Image 
                  source={Images.smileIcon}
                  size={10}
                  // style={{height:width/6,width:width/6}}
                />
              </TouchableOpacity>
              </View>
            </View>
        </View>
        <View style={{height:height/4,borderBottomColor:'gray',borderBottomWidth:.5}}>
          <View style={{
            marginLeft:width/20,
            flex:1,
            marginRight:width/20}}>
            <Text style={{color:'#4a4a4a',fontWeight:'600'}}>Pesanan Anda</Text>
          <ScrollView 
          {...this._panResponder.panHandlers}
          onScrollEndDrag={() => this.fScroll.setNativeProps({ scrollEnabled: true })} >
          {this.state.markers.map((marker,index)=>(
            <View style={{
              flexDirection:'row',
              flex:1,
              marginBottom:width/20,
              }}>
              <View style={{flex:1,}}>
              <Image 
              style={{width:width/5,height:width/5,borderRadius:3,marginRight:5}}
              source={marker.image}
              />
              </View>
              <View style={{flex:3,}}>
                <View style={{flexDirection:'column'}}>
                  <View style={{flex:1,height:height/20}}>
                    <Text style={{color:'#4a4a4a'}}>{marker.description}</Text>
                  </View>
                  <View style={{flex:1,height:height/20,flexDirection:'row',}}>
                    <View style={{flex:2}}>
                      <Text style={{color:'#4a4a4a'}}>Rp {marker.price},-</Text>
                    </View>
                    <View style={{flex:2,flexDirection:"row",}}>
                      <View style={{flex:.5,justifyContent:'flex-end',alignItems:'flex-end'}}>
                      <Image
                          source={Images.wishlistIcon}
                          size={10}
                        />
                      </View>
                      <View style={{flex:.3,alignSelf:"center"}}>
                      <Text style={{textAlign:'right'}}>2 Pcs</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
              ))}
          </ScrollView>
        </View>
        </View>
        <View style={{
          // height:height/3,
          flex:1,
          marginLeft:width/20,
          marginRight:width/20}}>
        <View style={{height:height/25,}}>
          <Text style={{fontWeight:'600',color:'#4a4a4a'}}>Ringkasan Transaksi</Text>
        </View>
        <View style={{height:height/25}}>
          <Text style={{color:'#4a4a4a'}}>Order no. 1237784823AB</Text>
        </View>
        <View style={{height:height/20,marginBottom:width/20}}>
              <View style={{flex:1,flexDirection:'row',}}>
                <View style={{ flex:1,flexDirection:'row'}}>
                  <View style={{flex:1,justifyContent:'flex-start',alignItems:'flex-start'}}>
                  <Image 
                    source={Images.currencyIcon}
                    size={3}
                    style={{alignSelf:'center',justifyContent:'center'}}
                  />
                  </View>
                <View style={{flex:3,justifyContent:'center',alignItems:'flex-start',}}>
                <Text style={{color:'#4a4a4a'}}>Subtotal</Text>
                </View>
                </View>
                <View style={{ flex:1,flexDirection:'row'}}>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'#4a4a4a'}}>Rp</Text>
                </View>
                <View style={{borderColor:'#454545',borderRadius:5,borderWidth:1,justifyContent:'center',alignItems:'center',flex:3}}>
                <Text style={{color:'#4a4a4a'}}>100.000</Text>
                </View>
                </View>
              </View>
            </View>
            <View style={{height:height/20,marginBottom:width/20}}>
              <View style={{flex:1,flexDirection:'row',}}>
                <View style={{ flex:1,flexDirection:'row'}}>
                  <View style={{flex:1,justifyContent:'flex-start',alignItems:'flex-start'}}>
                  <Image 
                    source={Images.deliveryIcon}
                    size={3}
                    style={{alignSelf:'center',justifyContent:'center'}}
                  />
                  </View>
                <View style={{flex:3,justifyContent:'center',alignItems:'flex-start',}}>
                <Text style={{color:'#4a4a4a'}}>Biaya Transport</Text>
                </View>
                </View>
                <View style={{ flex:1,flexDirection:'row'}}>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'#4a4a4a'}}>Rp</Text>
                </View>
                <View style={{borderColor:'#454545',borderRadius:5,borderWidth:1,justifyContent:'center',alignItems:'center',flex:3}}>
                <Text style={{color:'#4a4a4a'}}>100.000</Text>
                </View>
                </View>
              </View>
            </View>
            <View style={{height:height/20,marginBottom:width/20}}>
              <View style={{flex:1,flexDirection:'row',}}>
                <View style={{ flex:1,flexDirection:'row'}}>
                  <View style={{flex:1,justifyContent:'flex-start',alignItems:'flex-start'}}>
                  <Image 
                    source={Images.respectIcon}
                    size={3}
                    style={{alignSelf:'center',justifyContent:'center'}}
                  />
                  </View>
                <View style={{flex:3,justifyContent:'center',alignItems:'flex-start',}}>
                <Text style={{color:'#4a4a4a'}}>Tip Helper</Text>
                </View>
                </View>
                <View style={{ flex:1,flexDirection:'row'}}>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'#4a4a4a'}}>Rp</Text>
                </View>
                <View style={{borderColor:'#454545',borderRadius:5,borderWidth:1,justifyContent:'center',alignItems:'center',flex:3}}>
                <Text style={{color:'#4a4a4a'}}>100.000</Text>
                </View>
                </View>
              </View>
            </View>
            <View style={{height:height/20,marginBottom:width/20}}>
              <View style={{flex:1,flexDirection:'row',}}>
                <View style={{ flex:1,flexDirection:'row'}}>
                  <View style={{flex:1,justifyContent:'flex-start',alignItems:'flex-start'}}>
                  <Image 
                    source={Images.guaranteeIcon}
                    size={3}
                    style={{alignSelf:'center',justifyContent:'center'}}
                  />
                  </View>
                <View style={{flex:3,justifyContent:'center',alignItems:'flex-start',}}>
                <Text style={{color:'#4a4a4a'}}>Total</Text>
                </View>
                </View>
                <View style={{ flex:1,flexDirection:'row'}}>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'#4a4a4a'}}>Rp</Text>
                </View>
                <View style={{borderColor:'#454545',borderRadius:5,borderWidth:1,justifyContent:'center',alignItems:'center',flex:3}}>
                <Text style={{color:'#4a4a4a'}}>100.000</Text>
                </View>
                </View>
              </View>
            </View>
            <Text style={{fontWeight:'600',color:'#4a4a4a'}}>Dibayar dengan</Text>
            <View style={{height:height/20,marginBottom:width/20,}}>
              <View style={{flexDirection:'row',}}>
                {/* <View style={{ flex:1,flexDirection:'row'}}> */}
                  <View style={{flex:1,}}>
                  <Image 
                    source={Images.tcashIcon}
                    size={3}
                    // style={{alignSelf:'center',justifyContent:'center'}}
                  />
                  </View>
                {/* </View> */}
                <View style={{ flex:1,flexDirection:'row'}}>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'#4a4a4a'}}>Rp</Text>
                </View>
                <View style={{borderColor:'#454545',borderRadius:5,borderWidth:1,justifyContent:'center',alignItems:'center',flex:3}}>
                <Text style={{color:'#4a4a4a'}}>100.000</Text>
                </View>
                </View>
              </View>
            </View>
        </View>
      </ScrollView>
      </View>
      
      <View style={styles.footerContainer}>
          <View style={styles.footerContent}>
            <TouchableOpacity onPress={()=>{this.goToBeranda()}}>
              <Text style={{textAlign:'center',color:'#fff'}}>Kirim Penilaian</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'center',
    flexDirection:'column'
  },
  content:{
    top:0,
      height:height/15,
      backgroundColor:'#ffffff',
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
  },
  verifiedBox:{
    height:height/23,
    borderColor:'gray',
    borderWidth:1,
    borderRadius:5,
    justifyContent:"center",
    alignItems:'center'
  },
  footerContainer:{
    height:height/11.5,
    width:width,
    backgroundColor:'#ffffff',
    bottom:0,
    position:'absolute',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 5,
  },
  footerContent:{
    position:'relative',
    height:30,
    backgroundColor:'#fb4a6f',
    width:width/1.2,
    alignSelf:'center',
    borderRadius:5,
    margin:10,
    justifyContent:'center'
  }
  
});

const mapStateToProps = (state) => {
  return {
    // indicatorLogin: state.filterData.indicatorLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FinishOrder)
