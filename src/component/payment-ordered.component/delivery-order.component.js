import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  TextInput
} from 'react-native';
import { connect } from 'react-redux';
import Images from '@assets/images';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
type Props = {};
class DeliveryOrder extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  componentWillMount(){
  }

  render() {
    return(
      <View style={{height:height/4,borderBottomColor:'gray',borderBottomWidth:0.5,}}>
        <Text style={{left:'5%',fontWeight:'bold',color:'#4a4a4a'}}>Antar makanan ke</Text>
        <View style={{flexDirection:'row',paddingBottom:10}}>
          <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            <Image
              source={Images.roundIcon}
              style={{height:width/15,width:width/32,}}/>
          </View>
          <View style={{flex:6,borderColor:'#9b9b9b',height:height/18.9,borderRadius:3,borderWidth:0.3,marginBottom:5}}>
            <TextInput 
            style={{height:height/18,justifyContent:'center'}}
              placeholder='Telkomsel Smart Office'
              underlineColorAndroid='transparent' 
            />
          </View>
          <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>                
          </View>
        </View>
          <View style={{flexDirection:'row',flex:1,paddingBottom:10}}>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <Image
                source={Images.wishlistIcon}
                style={{height:width/15,width:width/15,}}
              />
            </View>
            <View style={{flex:6,borderColor:'#9b9b9b',borderRadius:3,borderWidth:0.3,padding:5,marginBottom:5}}>
              <TextInput 
              placeholder='ex:antar di gang Kelinci'
              multiline={true}
              underlineColorAndroid='transparent'
              numberOfLines={4}
              />
            </View>
            <View style={{flex:1}}>
            </View>
          </View>
          </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const mapStateToProps = (state) => {
  return {
    // indicatorLogin: state.filterData.indicatorLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryOrder)
