import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import {CheckBox} from 'react-native-elements';
import { connect } from 'react-redux'
import Images from '@assets/images';
import {RadioButton} from '@up-shared/components'
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
type Props = {};
class HowToPay extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      radioItems: [{
        label: <Image source={Images.tcashIcon}style={{height:60,width:180}}/>,
        size: 30,
        color: '#fb4a6f',
        selected: false,
        value: 'tcash'
    },{
        label: 'Tunai',
        size: 30,
        color: '#fb4a6f',
        selected: false,
        value:'tunai'
        },            
      ], selectedItem: '' }
    }
  componentDidMount(){
    this.state.radioItems.map(( item ) =>
    {
        if( item.selected == true )
        {
            this.setState({ selectedItem: item.value });
        }
    });
  }
  changeActiveRadioButton(index){
    this.state.radioItems.map(( item ) =>
    {
        item.selected = false;
    });
 
    this.state.radioItems[index].selected = true;
 
    this.setState({ radioItems: this.state.radioItems }, () =>
    {
        this.setState({ selectedItem: this.state.radioItems[index].value });
    });
  }
  render() {
    return(
      <View>
        <Text style={{color:'#4a4a4a',left:'5%',fontWeight:'bold'}}>Pilih cara bayar Anda</Text>
          <View style={{flexDirection:'row',flex:3,}}>
            <View style={{flex:1,flexDirection:'row',paddingBottom:15}}>
              <View style={{flex:0.15}}></View>
                <View style={{flex:1,justifyContent:'center',alignContent:'center',}}>
                { 
                  this.state.radioItems.map(( item, key ) =>(
                <RadioButton key = { key } button = { item } onClick = { this.changeActiveRadioButton.bind( this, key ) }/>
                  ))
                }
                </View>
              </View>
            <View style={{flex:1,flexDirection:'row',}}>
              <View style={{flex:1 ,}}>
                <Text>Saldo Anda: </Text>
              </View>
              {/* <View style={{borderRadius:100,backgroundColor:'red'}}></View> */}
              <View style={{flex:1,}}>
                <Text>Rp 90000</Text>
              </View>              
            </View>
          </View>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25
  },
  radioButton:{
    flexDirection: 'row',
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  radioButtonHolder:{
    borderRadius: 50,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  radioIcon:{
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },

  label:{
    marginLeft: 10,
    fontSize: 20
  },

  selectedTextHolder:{
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    padding: 15,
    backgroundColor: 'rgba(0,0,0,0.6)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  selectedText:{
    fontSize: 18,
    color: 'white'
  }
});

const mapStateToProps = (state) => {
  return {
    // indicatorLogin: state.filterData.indicatorLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HowToPay)
