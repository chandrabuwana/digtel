import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import Images from '@assets/images';
import formatRupiah from '@helpers/IDRFormat'
import {postPlaceOrder} from '../../actions/index'
// import TipHelper from './tip-helper.component';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
type Props = {};
class AppAndro extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      indikasi: 1,
      totalPembelian:[],
      hitungPembelian:[],
      dataCoba:[],
      dataIsi:[],
      cartTanpaPembelian:[],
      biayaTransport:0,
    }
  }
  componentDidMount(){
    // alert(JSON.stringify(this.props.dataChoosen))
    this.setState({dataCoba:this.props.dataChoosen},()=>{
      this.hitungHitung(this.state.dataCoba)
    })
  }
  componentWillMount(){
    
  }

  functionButton(data) {
    this.setState({indikasi: data})
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.dataSubtotalPrice){
      this.setState({totalPembelian:nextProps.dataSubtotalPrice}, () => {
        this.hitungPembelian(nextProps.dataSubtotalPrice)
      })
    }
  }
  hitungHitung(value){
    // alert(JSON.stringify(value))
    let dataIki = value;
    let dataqtyIsi =[]
    for (let i = 0; i < dataIki.length; i++) {
      if(dataIki[i].qwt!=0){
        dataqtyIsi.push(dataIki[i])
        this.setState({dataIsi:dataqtyIsi},() =>{
          this.hitungPembelian(this.state.dataIsi)
        })
      }
      else{
        this.setState({cartTanpaPembelian:dataqtyIsi})
      }
      
    }
  }
  hitungPembelian (value){
    // this.props.postPlaceOrder(value)
    // alert(JSON.stringify(value))
    let kosong=[]
    let data2= value
    let dataSementara = 0
    for (let i = 0; i < data2.length; i++) {
      let hitung = Number(data2[i].qwt)*Number(data2[i].price)
      dataSementara += hitung
    }
    
    this.setState({hitungPembelian:dataSementara})
  }
  
  render() {
    return(
      <View style={{flex:2,flexDirection:'column'}}>
        <View style={{height:height/25}}>
          <Text style={{left:'5%',fontWeight:'bold'}}>Ringkasan Transaksi</Text>
        </View>
        <View style={{flexDirection:'row',flex:1.5,}}>
          <View style={{flex:1,flexDirection:'row',paddingBottom:15}}>
          <View style={{flex:1}}></View>
            <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
            <Image
              source={Images.currencyIcon}
              size={15}
            />
            </View>
            <View style={{flex:4,justifyContent:'center'}}>
              <Text style={{textAlign:'left',color:'#4a4a4a',left:'4%'}}>Subtotal</Text>
            </View>
          </View>
          <View style={{flex:1,flexDirection:'row'}}>
            <View style={{flex:1}}>
              
            </View> 
            <View style={{flex:2,height:height/25,borderColor:'black',borderWidth:0.5,borderRadius:5,padding:5}}>
              <Text>{formatRupiah(Number(this.state.hitungPembelian))}</Text>
            </View>
            <View style={{flex:1}}></View>
          </View>
        </View>
        <View style={{flexDirection:'row',flex:1.5}}>
          <View style={{flex:1,flexDirection:'row',paddingBottom:15}}>
          <View style={{flex:1}}></View>
            <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
            <Image
              source={Images.deliveryIcon}
              size={10}
              // style={{height:height/30,width:width/15,}}
            />
            </View>
            <View style={{flex:4,}}>
              <Text style={{left:'4%'}}>Biaya Transport</Text>
            </View>
          </View>
          <View style={{flex:1,flexDirection:'row'}}>
            <View style={{flex:1}}>
            </View>
            <View style={{height:height/25,flex:2,borderColor:'black',borderWidth:0.5,borderRadius:5,padding:5}}>
              
                <Text>{this.state.biayaTransport}</Text>

              </View>
            <View style={{flex:1}}></View>
          </View>
        </View>
        {/* <TipHelper/> */}
        <View style={{flexDirection:'row',flex:1.5,paddingBottom:15}}>
          <View style={{flex:1,flexDirection:'row',}}>
          <View style={{flex:1}}></View>
            <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
            <Image
              source={Images.respectIcon}
              size={20}
              // style={{height:width/18,width:width/17,}}
            />
            </View>
            <View style={{flex:4}}>
              <Text style={{left:'4%'}}>Tip Helper</Text>
            </View>
          </View> 
          <View style={{flex:1,flexDirection:'row'}}>
            
            <View style={{flex:1}}></View>
          </View>
        </View>

        <View style={{flexDirection:'row',flex:1.5,paddingBottom:15}}>
          <View style={{flex:.5}}></View>
          {
          this.state.indikasi === 1 ? 
            <TouchableOpacity style={{height:height/25,paddingRight:width/45}} onPress={() => this.functionButton(1)}>
            <View style={styles.onStyle}>                      
              <Text style={{
                color:'#4a4a4a',
                textAlign:'center'}}>Rp 5000</Text>
            </View>
          </TouchableOpacity> :
          <TouchableOpacity style={{height:height/25,paddingRight:width/45}} onPress={() => this.functionButton(1)}>
            <View style={styles.offStyle}>                      
              <Text style={{
                color:'#4a4a4a',
                textAlign:'center'
                }}>Rp 5000</Text>
            </View>
          </TouchableOpacity>
          }
          {
            this.state.indikasi === 2 ?
            <TouchableOpacity style={{height:height/25,paddingRight:width/45}} onPress={() => this.functionButton(2)}>
              <View style={styles.onStyle}>                      
                <Text style={{color:'#4a4a4a',textAlign:'center'}}>Rp 10.000</Text>
              </View>
            </TouchableOpacity> :
            <TouchableOpacity style={{height:height/25,paddingRight:width/45}} onPress={() => this.functionButton(2)}>
              <View style={styles.offStyle}>                      
                <Text style={{color:'#4a4a4a',textAlign:'center'}}>Rp 10.000</Text>
              </View>
            </TouchableOpacity>
          }
          {
            this.state.indikasi === 3 ?
            <TouchableOpacity style={{height:height/25,paddingRight:width/45}} onPress={() => this.functionButton(3)}>
              <View style={styles.onStyle}>                      
                <Text style={{color:'#4a4a4a',textAlign:'center'}}>Rp 15000</Text>
              </View>
            </TouchableOpacity> :
            <TouchableOpacity style={{height:height/25,paddingRight:width/45}} onPress={() => this.functionButton(3)}>
              <View style={styles.offStyle}>                      
                <Text style={{color:'#4a4a4a',textAlign:'center'}}>Rp 15000</Text>
              </View>
            </TouchableOpacity>
          }
          
          <View style={{flex:1}}></View>
        </View>

        <View style={{flexDirection:'row',flex:1.5,paddingBottom:15}}>
          <View style={{flex:1,flexDirection:'row',paddingBottom:15}}>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            
            </View>
            <View style={{flex:4}}>
              <Text>Atau tentukan tipmu</Text>
            </View>
          </View>
          <View style={{flex:1,flexDirection:'row'}}>
            <View style={{flex:1}}>
              <Text>Rp</Text>
            </View>
            <View style={{height:height/18.5,flex:2,borderColor:'black',borderWidth:0.5,borderRadius:5,padding:5}}>
              
            <TextInput 
            keyboardType='numeric'
              textAlign='left'
              style={{height:height/18,}}
                placeholder='100.000'
                underlineColorAndroid='transparent' 
              />

              </View>
            <View style={{flex:1}}></View>
          </View>
        </View>
        <View style={{flexDirection:'row',flex:1.5}}>
          <View style={{flex:1,flexDirection:'row',paddingBottom:15}}>
            <View style={{flex:1}}></View>
            <View style={{flex:1.5,justifyContent:'center',alignItems:'center'}}>
            <Image
              source={Images.guaranteeIcon}
              // style={{height:height/30,width:width/15,}}
              size={10}
            />
            </View>
            <View style={{flex:4}}>
              <Text>Total</Text>
            </View>
          </View>
          <View style={{flex:1,flexDirection:'row'}}>
            <View style={{flex:1}}>
              <Text>Rp</Text>
            </View>
            <View style={{height:height/25,flex:2,borderColor:'black',borderWidth:0.5,borderRadius:5,padding:5}}>
              <Text>90000</Text>
            </View>
            <View style={{flex:1}}></View>
          </View>
        </View>
    </View> 
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  onStyle:{
    flex:1,
    backgroundColor:'#ffbfcc',
    borderRadius:5,
    width:width/5,
    borderColor:'#fb4a6f',
    borderWidth:1,
    justifyContent:'center',
  },
  offStyle:{
    flex:1,
    backgroundColor:'white',
    borderRadius:5,
    borderColor:'#4a4a4a',
    borderWidth:1,
    width:width/5,
    justifyContent:'center',
  },
});

const mapStateToProps = (state) => {
  return {
    // indicatorLogin: state.filterData.indicatorLogin
    
    dataSubtotalPrice: state.filterData.dataSubtotalPrice,
    dataFromChooseFood: state.filterData.dataFromChooseFood,
    dataChoosen: state.filterData.dataChoosen,
    
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    postPlaceOrder: (data) => { dispatch( postPlaceOrder(data) ) },
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppAndro)
