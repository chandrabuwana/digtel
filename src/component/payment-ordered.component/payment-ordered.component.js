import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Image,
  TouchableNativeFeedback,
  TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { connect } from 'react-redux';
import Images from '@assets/images';
import DeliveryOrder from './delivery-order.component';
import ListOrdered from './list-ordered.component';
import {
  fetchProductFromMerchant,
  fetchProductFromMerchantDup,
  dataAfterChooseFood, 
  fetchAgainProductFromMerchant,
  fetchFromChooseFood,
  subtotalPrice,
  postPlaceOrder,
  readyForOrder
} from '../../actions/index';
import SummaryOfTransaction from './summary-of-transaction.component';
import HowToPay from './how-to-pay.component';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
type Props = {};

class PaymentOrdered extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      dataDariSana:[],
      cartBelanja: [],
      coba:[],
      data1:[],
      datakosong:[]
    }
  }
  
  
  render() {
    return(
      <View style={styles.container}>
        <View style={styles.header}>        
          <View style={{flexDirection:"row",flex:1,}}>
            <View style={{width:40}}>
              <View style={{flex:1,alignItems:'center',justifyContent:'center',}}>                  
                <TouchableOpacity onPress={()=>{this.goBacktoFoodChoose()}}>
                <Image 
                  source={Images.arrowBackIcon}
                  style={{width:width/22,height:width/22,alignSelf:'center'}}                  
                />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{flex:2,alignSelf:'center',justifyContent:'flex-start',}}>
              <Text style={{fontSize:width/25,fontWeight:'bold',left:'5%',color:'#454545',fontFamily:'Montserrat-Italic'}}>Beli Makanan</Text>
            </View>
          </View>
        </View>
        <ScrollView style={{marginBottom:height/15}}>
          <View>
          <Text style={{left:'5%',fontWeight:'bold',color:'#4a4a4a'}}>Pesanan Anda</Text>
            {this.state.cartBelanja ? this.state.cartBelanja.map((data,index)=>{
              return(
                <ListOrdered datas={data}
                  tambahBelanja={(val) => this.tambahBelanja(val)}
                  hapusBarang={(val) => this.hapusBarang(val)}
                />
              )
            }):null} 
            <View style={{height:height/15,backgroundColor:'#fb4a6f',justifyContent:'center'}}>
          <TouchableOpacity>
            <Text style={{textAlign:'center',color:'white',fontSize:height/40}}>Tambah Pesanan</Text>
          </TouchableOpacity>
        </View>
          </View>
          <DeliveryOrder/>
          <SummaryOfTransaction/>
          <HowToPay/>
        </ScrollView>
        <View style={styles.footerView1}>
          <View style={styles.footerView2}>
            <TouchableOpacity onPress={()=>{this.goToSearchHelper()}}>
              <Text style={{textAlign:'center',color:'#fff'}}>Pesan</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
  componentWillMount(){
    this.dataDariFoodChoose()
    this.setState({dataDariSana: this.props.dataChoosen})
  }
  componentDidMount(){
    // alert(JSON.stringify(this.props.dataFromMaps))
    this.hitungJumlahPembelian()
  }
  goBacktoFoodChoose(){
    // this.props.postPlaceOrder()
    this.props.navigation.navigate('FoodChoose')
  }
  goToSearchHelper(){
    this.postData()
    this.props.navigation.navigate('SearchHelper')
  }

  dataDariFoodChoose(){
    let data1 =this.props.dataChoosen;
    let dataState =this.state.dataDariSana;
    this.setState({dataDariSana:dataState})
    let tamp1=[]
    for (let i = 0; i < data1.length; i++) {
      if(data1[i].qwt != 0){
        tamp1.push(data1[i]);
        this.setState({cartBelanja:tamp1});
        this.props.readyForOrder(tamp1)
      }else{
        this.setState({dataDariSana: this.props.dataChoosen})
      }
    }
  }
  postData(){

    this.props.postPlaceOrder(this.props.dataReadyForOrder)
  }
  tambahBelanja (val){
    let tamp =[]
    let dataa = this.props.dataFromChooseFood
    let idBelanja = val.id
    let dataAwal = this.state.dataDariSana
    let dataIndex = dataAwal.findIndex((dataIndexs) => {
      return dataIndexs.id === idBelanja
    })
    dataAwal.splice(dataIndex, 1, val)
    this.props.fetchAgainProductFromMerchant(dataAwal)
    this.maksaAnjing(dataAwal)
    this.props.fetchFromChooseFood(dataAwal)
  }

  hapusBarang(val){
    if(val.qwt === 0){
    } else{
      val.qwt -=1
      let idBelanja = val.id
      let dataAwal = this.state.dataDariSana
      let dataIndex = dataAwal.findIndex((dataIndexs) => {
        return dataIndexs.id === idBelanja
      })
      dataAwal.splice(dataIndex, 1, val)
      this.maksaAnjing(dataAwal)
      this.props.fetchAgainProductFromMerchant(dataAwal)
      this.props.fetchFromChooseFood(dataAwal)
    }
  }

  maksaAnjing(value) {
    let datadata=[]
    this.setState({dataDariSana: value})
    for (let i = 0; i < value.length; i++) {
      if(value[i].qwt!=0){
        datadata.push(value[i])
        
        this.setState({data1:datadata},()=>{
          this.props.subtotalPrice(datadata)
          this.props.readyForOrder(datadata)
        })
      }else{
        this.setState({data1:datadata},()=>{
          this.props.subtotalPrice(datadata)
        })
      }
    }
  }
  
  hitungJumlahPembelian (nextProps){
    
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'column',
    backgroundColor:'#ffffff'
  },
  header: {
    backgroundColor:'#ffffff',
    height:height/15,    
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 5,
    position:'relative',
  },
  content:{
    backgroundColor:'#ffffff',
    flex:10,
    flexDirection:'column'
  },
  footerView1:{
    height:height/11.5,
    width:width,
    backgroundColor:'#ffffff',
    bottom:0,
    position:'absolute'
  },
  footerView2:{
    position:'relative',
    height:30,
    backgroundColor:'#fb4a6f',
    width:width/1.2,
    alignSelf:'center',
    borderRadius:5,
    margin:10,
    justifyContent:'center'
  }
});

const mapStateToProps = (state) => {
  return {
    dataChoosen: state.filterData.dataChoosen,
    merchantDataChoosen: state.filterData.merchantDataChoosen,
    dataFromBuyFood: state.filterData.dataFromBuyFood,
    dataFromChooseFood: state.filterData.dataFromChooseFood,
    dataSubtotalPrice:state.filterData.dataSubtotalPrice,
    dataReadyForOrder:state.filterData.dataReadyForOrder,
    // dataFromMaps:state.filterData.dataFromMaps,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {

    fetchProductFromMerchant: (data) => { dispatch( fetchProductFromMerchant(data) ) },
    fetchProductFromMerchantDup: (data) => { dispatch( fetchProductFromMerchantDup(data) ) },
    dataAfterChooseFood: (data) => { dispatch( dataAfterChooseFood(data) ) },
    fetchAgainProductFromMerchant: (data) => { dispatch( fetchAgainProductFromMerchant(data) ) },
    fetchFromChooseFood: (data) => { dispatch( fetchFromChooseFood(data) ) },
    subtotalPrice: (data) => { dispatch( subtotalPrice(data) ) },
    postPlaceOrder: (data) => { dispatch( postPlaceOrder(data) ) },
    readyForOrder: (data) => { dispatch( readyForOrder(data) ) },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentOrdered)
