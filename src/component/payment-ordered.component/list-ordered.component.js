import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions

} from 'react-native';
import { connect } from 'react-redux'
import Images from '@assets/images';
import formatRupiah from '../../helpers/IDRFormat';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
var { height,width } = Dimensions.get('window');
class ListOrdered extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  componentWillMount(){
  }
  tambahBelanja(val){
    val.qwt +=1
    this.props.tambahBelanja(val)
  }

  hapusBarang(val) {
    this.props.hapusBarang(val)
  }
  counterIncrease(){
    this.setState({count:this.state.markers[0].count+1})
  }
counterDecrease(){
  this.setState({count:this.state.markers[0].count-1})
}
  render() {
    return(
      <View style={{flex:1}}>
        <View style={{
          padding:10,
          flexDirection:'row',
          flex:1,
          backgroundColor: '#ffffff',
          }}>
          <View style={{flex:.5}}></View>
          <View style={{flex:1,paddingRight:width/10}}>
            <Image 
            style={{width:width/5,height:width/5,borderRadius:3}}
            source={{uri:this.props.datas.desc}}
            />
          </View>
          <View style={{flex:6}}>
            <View style={{flexDirection:'column'}}>
              <View style={{flex:1,height:height/20}}>
                <Text style={{color:'#4a4a4a'}}>{this.props.datas.name}</Text>
              </View>
              <View style={{flex:1,height:height/20,flexDirection:'row'}}>
                <View style={{flex:1}}>
                  <Text style={{color:'#4a4a4a'}}>{formatRupiah(Number(this.props.datas.price))}</Text>
                </View>
                <View style={{flex:2,flexDirection:"row",}}>
                  <View style={{flex:1.5,justifyContent:'flex-end',alignItems:'flex-end'}}>
                    <Image
                        source={Images.wishlistIcon}
                        style={{height:width/15,width:width/15,}}
                      />
                  </View>
                  <View style={{flex:1,justifyContent:'flex-end',alignItems:'flex-start'}}>
                    <TouchableOpacity onPress={()=>{this.hapusBarang(this.props.datas)}}>
                    <Image
                      source={Images.minusSymbolIcon}
                      style={{height:height/30,width:height/30}}
                    />
                    </TouchableOpacity>
                  </View>
                  <View style={{flex:1,borderColor:'gray',borderWidth:.5,padding:5,justifyContent:'center',alignItems:'center',alignSelf:'center'}}>
                    <Text style={{textAlign:'center'}}>{this.props.datas.qwt}</Text>
                  </View>
                  <View style={{flex:1,justifyContent:'flex-end',alignItems:'flex-end'}}>
                    <TouchableOpacity onPress={()=>{this.tambahBelanja(this.props.datas)}}>
                    <Image
                      source={Images.plusButtonIcon}
                      style={{height:height/30,width:height/30}}
                    />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
    </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const mapStateToProps = (state) => {
  return {
    dataChoosen: state.filterData.dataChoosen
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListOrdered)
