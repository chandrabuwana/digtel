import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux'
import Images from '@assets/images';
import Checkbox from 'react-native-modest-checkbox'
import { RadioButton } from '../../shared/components';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
type Props = {};
// class ButtonGroup extends Component{
//   constructor(){
//     super()
//   }
//   render(){
//     return(
//       // <View>
//       <TouchableOpacity 
//       onPress = { this.props.onClick } 
//       activeOpacity = { 0.8 } 
//       style = { styles.radioButton }>
      
//           <View 
//           style = {[ styles.radioButtonHolder, 
//           { height: this.props.button.height, 
//           width: this.props.button.width, 
//           borderColor: this.props.button.activeColor}]}>
//           {
//             (this.props.button.selected)
//             ?
//             (<View 
//               style = {[ styles.radioIcon, 
//                 { height: this.props.button.height,
//                   width: this.props.button.width, 
//                   backgroundColor: this.props.button.activeColor}]}></View>)
//             :
//             null
//           }
//           <Text style = {[ styles.label, 
//           { color: this.props.button.activeColor
//           }]}>{ this.props.button.label }</Text>
//           </View>
//         </TouchableOpacity>
//     )
//   }
// }
class TipHelper extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      groupItems: [{
        label: 5000,
        height:height/25,
        width:width/5,
        inactiveColor:'#ffff',
        activeColor: '#fb4a6f',
        
        selected: false,
    },{
        label: 10000,
        height:height/25,
        width:width/5,
        activeColor: '#fb4a6f',
        inactiveColor:'#ffff',
        selected: false,
        }, {
          label: 15000,
          height:height/25,
          width:width/5,
          activeColor: '#fb4a6f',
          inactiveColor:'#ffff',
          selected: true,          
      },             
      ], selectedItem: '' }
    }
  
  componentWillMount(){
  }
  componentDidMount(){
    
    this.state.groupItems.map(( item ) =>{
      if( item.selected == true ){
        this.setState({ selectedItem: item.label });
      }
    });
  }
 
    changeActiveRadioButton(index){
        this.state.groupItems.map(( item ) =>{
            item.selected = false;
        });
        this.state.groupItems[index].selected = true;
        this.setState({ groupItems: this.state.groupItems }, () =>{
          this.setState({ selectedItem: this.state.groupItems[index].inactiveColor });
        });
    }

  render() {
    return(
      <View style={styles.container}>
        <View style={{flexDirection:'row',flex:1.5,paddingBottom:15}}>
          <View style={{flex:1,flexDirection:'row',}}>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            <Image
              source={Images.respectIcon}
              size={10}
              // style={{height:height/30,width:width/15,}}
            />
            </View>
            <View style={{flex:4}}>
              <Text>Tip Helper</Text>
            </View>
          </View> 
          <View style={{flex:1,flexDirection:'row'}}>
            
            <View style={{flex:1}}></View>
          </View>
        </View>
        <View style={{flexDirection:'row',flex:1}}>
        <Checkbox
          checkedComponent={
          <Text style={{color:'black',textAlign:'center'}}>Rp 5000</Text>
          }
  uncheckedComponent={
  <Text style={{color:'black',textAlign:'center'}}>Rp 15000</Text>
} 
  onChange={(checked) => console.log('Checked!')}
/>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // paddingHorizontal: 25
  },
  // groupButtonHolder:{
  //   justifyContent:'center',
  //   alignItems:'center',
  //   flex:1,
  //   padding:20,
  //   // paddingRight:width/45,
  //   borderColor:'gray',
  //   backgroundColor:'yellow'
  // },
  // selectedButton:{
  //   // backgroundColor:this.state.selectedItem.color,
  //   flexDirection:'row',
  //   height:height/25,
  //   width:width/5,
    
  //   borderColor:'gray',
  //   borderWidth:.5,
  //   justifyContent:'center',
  //   alignItems:'center'
  // }
  radioButton:{
    flexDirection: 'row',
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center'
},

radioButtonHolder:{
  borderRadius: 50,
  borderWidth: 1,
  justifyContent: 'center',
  alignItems: 'center'
},
radioIcon:{
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center'
},
});


const mapStateToProps = (state) => {
  return {
    // indicatorLogin: state.filterData.indicatorLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TipHelper)
