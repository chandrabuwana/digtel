import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Button,
  TouchableOpacity,
  Text,
  View,
  Image,
  ScrollView,
  StatusBar,
  PermissionsAndroid,
  Modal,
  Dimensions,
  Animated
} from 'react-native';
import Images from '@assets/images'
import { connect } from 'react-redux'
import MapsHome from '../maps-home.component/maps-home.component'
import SlidingUpPanel from 'rn-sliding-up-panel';
import Icon from 'react-native-vector-icons/FontAwesome5'
import CustomIcon from '../CustomIcon'
import Menu from '../modal/menu.modal.component/menu.modal.component'
import Ordered from '../ordered.compononet/ordered.component'

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
const {height,width} = Dimensions.get('window')

type Props = {};
class Home extends Component<Props> {
  static defaultProps = {
    draggableRange: {
      //untuk ngatur naik turunnya bottom sheet
      top: height / 3,
      bottom: 200
    }
  }

  _draggedValue = new Animated.Value(-190)

  constructor(props) {
    super(props);    
    this.state = {
      indicatorModal: false, 
      indicatorModalMenu:false,
      visible: false
     }
  }
  showModalMenu () {
    this.setState({indicatorModalMenu: !this.state.indicatorModalMenu})
  } 
  showActionSheet = () => {
    this.ActionSheet.show()
  }
  goToBuyFoodPage() {
    this.props.navigation.navigate('BuyFood')
    
    // this.setState({indicatorModal: !this.state.indicatorModal})
  }
  componentDidMount(){
   
  }
  componentWillMount(){
  }
  static navigationOptions = {
    tabBarLabel: 'Home',    
  };
  
  render() { 
    return(
      <View style={styles.container}>        
        <View style={{flex:1}}>
          <View style={{flex:1,height:height,width:width}}>
            <MapsHome/>
          </View>
        </View>
        <SlidingUpPanel
          visible
          startCollapsed
          showBackdrop={false}
          ref={c => this._panel = c}
          draggableRange={this.props.draggableRange}
          onDrag={v => this._draggedValue.setValue(v)}>
          <View style={styles.panel}>
              <Image source={Images.rectangleIcon}
              style={{width: 40, height: 5,alignSelf:'center',marginTop:10}}
              />            
              <Text style={{left:'6%',fontWeight:'bold'}}>Pilih Layanan Yang kamu mau</Text>
            <View style={{flexDirection:'row',paddingTop:10,justifyContent:'center'}}>
              <TouchableOpacity onPress={()=> {this.goToBuyFoodPage()}}>
                <View style={{width:width/4,alignItems:'center',alignSelf:'center',paddingBottom:40,top:'9%'}}>
                <Image 
                source={Images.makananIcon}
                style={{width:width/9,height:width/9,alignSelf:'center'}}/>
                  <Text>Beli</Text>
                  <Text>Makanan</Text>
                </View>
              </TouchableOpacity>
                <View style={{width:width/4,alignItems:'center',alignSelf:'center',paddingBottom:40,top:'2.6%'}}>
                  {/* <CustomIcon name="dolly" size={20}></Icon> */}
                  <Image 
                    source={Images.belanjaIcon}
                    style={{width:width/9,height:width/9,alignSelf:'center'}}
                  />
                  <Text>Belanja</Text>
                  <Text>  </Text>
                </View>
                <View style={{width:width/4,alignItems:'center',alignSelf:'center',paddingBottom:40,top:'2.6%'}}>
                  {/* <CustomIcon name="box" size={20}></Icon> */}
                  <Image 
                    source={Images.bersihIcon}
                    style={{width:width/9,height:width/9,alignSelf:'center'}}
                  />
                  <Text>Bersih</Text>
                  <Text>Rumah</Text>
                </View>
                <View style={{width:width/4,alignItems:'center',alignSelf:'center',paddingBottom:40,}}>
                  {/* <CustomIcon name="broom" size={20}></Icon> */}
                  <Image 
                    source={Images.apaajaIcon}
                    style={{width:width/9,height:width/9,alignSelf:'center'}}
                  />
                  <Text>Apa Aja</Text>
                </View>
            </View>            
          </View>
        </SlidingUpPanel>       
      </View>
    )
  }
}

const styles = StyleSheet.create({
  modal: {
    // marginBottom:20,
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    margin: 0, 
    backgroundColor: '#ffff', 
    height: 100, 
    flex:0 , 
    bottom: 0, 
    position: 'absolute',
    width: '100%'
  },
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  panel: {
    flex: 1,
    backgroundColor: '#ffffff',
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    position: 'relative'
  },
  panelHeader: {
    flex:1,
    // flexDirection:'row',
    height: 60,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom:20
  },
  favoriteIcon: {
    position: 'absolute',
    top: -24,
    right: 24,
    backgroundColor: '#2b8a3e',
    width: 48,
    height: 48,
    padding: 8,
    borderRadius: 24,
    zIndex: 1
  }
});



const mapStateToProps = (state) => {
  return {
    // indicatorLogin: state.filterData.indicatorLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
