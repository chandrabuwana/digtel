import React, { Component } from 'react';
import { Platform,
  View,
  Text,
  Modal,
  StyleSheet,
  Dimensions,
  Slider,
  TouchableOpacity,
} from 'react-native';
// import Modal from 'react-native-modal';
import {connect} from 'react-redux'
const { width, height } = Dimensions.get("window");

const MODAL_HEIGHT= height/3;
const MODAL_WIDTH = MODAL_HEIGHT -20;
class FilterFood extends Component {
  constructor(props) {
    super(props);
    this.state = {
      range:0
    }
    
  }
  getVal(val){
    
    } 
  render() { 
    return ( 
      <View style={styles.container}>
          <Text>Jarak dari Lokasimu</Text>
        {/* <View style={styles.card}>
          <View style={{flexDirection:'row'}}>
            <View style={{flex:1}}>
            </View>
            <View style={{flex:1}}>
              <Text style={styles.rangeText}>{this.state.range}km</Text>
            </View>

          </View>
      <Slider
         style={{ width: 300 }}
         step={1}
         minimumValue={0}
         maximumValue={100}
         value={this.state.range}
         onValueChange={val => this.setState({ range: val })}
         onSlidingComplete={ val => this.getVal(val)}
         /> */}
        {/* ================= Helpers filter ================ */}
        {/* <Text>Helpers</Text>
        <View style={{flexDirection:'row',}}>
          <View style={{flex:1}}>
            <TouchableOpacity>
              <View style={styles.inerViewButton}>
                <View style={styles.subInerViewButton}>
                    <Text style = {styles.textButtonEdit}>Verified</Text>
                </View>
              </View>
            </TouchableOpacity>

          </View>
          <View style={{flex:1}}>
            <TouchableOpacity>
              <View style={styles.inerViewButton}>
                <View style={styles.subInerViewButton}>
                    <Text style = {styles.textButtonEdit}>All</Text>
                </View>
              </View>
            </TouchableOpacity>

          </View>
        </View> */}
        {/*====================  helpers Rating====================== */}
        {/* <Text>Helpers Rating</Text>
        <View style={{flexDirection:'row',}}>
          <View style={{flex:1}}>
            <TouchableOpacity>
              <View style={styles.inerViewButton}>
                <View style={styles.subInerViewButton}>
                    <Text style = {styles.textButtonEdit}>Bronze</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{flex:1}}>
            <TouchableOpacity>
              <View style={styles.inerViewButton}>
                <View style={styles.subInerViewButton}>
                    <Text style = {styles.textButtonEdit}>Gold</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{flex:1}}>
            <TouchableOpacity>
              <View style={styles.inerViewButton}>
                <View style={styles.subInerViewButton}>
                    <Text style = {styles.textButtonEdit}>Silver</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{flex:1}}>
            <TouchableOpacity>
              <View style={styles.inerViewButton}>
                <View style={styles.subInerViewButton}>
                    <Text style = {styles.textButtonEdit}>All</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          </View> */}

          {/* ============================Merchant ======================== */}
          
          {/* <View style={{flexDirection:'row'}}>
            <View style={{flex:1}}>
              <Text>Merchant</Text>
            </View>
            <View style={{flex:1}}>
              <Text>lihat semua</Text>
            </View>
          </View>
          <View style={{flexDirection:'row'}}>
            <View style={{flex:1}}>
              <TouchableOpacity>
                <View style={styles.inerViewButton}>
                  <View style={styles.subInerViewButton}>
                      <Text style = {styles.textButtonEdit}>Makanan</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{flex:1}}>
              <TouchableOpacity>
                <View style={styles.inerViewButton}>
                  <View style={styles.subInerViewButton}>
                      <Text style = {styles.textButtonEdit}>Sayur</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{flex:1}}>
              <TouchableOpacity>
                <View style={styles.inerViewButton}>
                  <View style={styles.subInerViewButton}>
                      <Text style = {styles.textButtonEdit}>Fast Food</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{flex:1}}>
              <TouchableOpacity>
                <View style={styles.inerViewButton}>
                  <View style={styles.subInerViewButton}>
                      <Text style = {styles.textButtonEdit}>All</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View> */}

          {/* ================Merchant Rating============== */}
          {/* <View style={{flexDirection:'row'}}>
            <View style={{flex:1}}>
              <Text>Merchant Rating</Text>
            </View>
            <View style={{flex:1}}>
              <Text>lihat semua</Text>
            </View>
            </View>
              <View style={{flexDirection:'row'}}>
                <View style={{flex:1}}>
                  <TouchableOpacity>
                    <View style={styles.inerViewButton}>
                      <View style={styles.subInerViewButton}>
                          <Text style = {styles.textButtonEdit}>Gold</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={{flex:1}}>
                  <TouchableOpacity>
                    <View style={styles.inerViewButton}>
                      <View style={styles.subInerViewButton}>
                          <Text style = {styles.textButtonEdit}>Silver</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={{flex:1}}>
                  <TouchableOpacity>
                    <View style={styles.inerViewButton}>
                      <View style={styles.subInerViewButton}>
                          <Text style = {styles.textButtonEdit}>Bronze</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={{flex:1}}>
                  <TouchableOpacity>
                    <View style={styles.inerViewButton}>
                      <View style={styles.subInerViewButton}>
                        <Text style = {styles.textButtonEdit}>All</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              
                <View style={{flex:1}}>
                  <TouchableOpacity>
                    <View style={styles.inerViewButton}>
                      <View style={styles.subInerViewButton}>
                        <Text style = {styles.textButtonEdit}>Terapkan</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>

              
          </View> */}
        </View>
     );
  }
}

const styles = StyleSheet.create({
  container: {
    // marginTop:20,
    flex:1,
    backgroundColor:'#fff',
    justifyContent:'center',
    alignItems: 'center',    
    // width:width-40,
    height:height,
  },
  card:{
    // height:MODAL_HEIGHT,
    // width: MODAL_WIDTH,
    padding:10,
    elevation:2
  },rangeText: {
    fontSize: 20,
    textAlign: 'right',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  inerViewButton: {
    // flex:1,
    alignItems:'center',
    justifyContent:'center',
  },
  subInerViewButton: {
    borderRadius:10,
    padding:5,
    // paddingLeft:90,
    // paddingRight:90,
    color:'white',
    backgroundColor:'#ffc817',
    alignContent:'center',
    alignItems:'center',
  },
})
const mapStateToProps = (state,) => {
  return {
   //  prop: state.prop
  }
}
const mapDispatchToProps = (dispatch,) => {
  return {
   //  dispatch1: () => {
   //    dispatch(actionCreator)
   //  }
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(FilterFood);