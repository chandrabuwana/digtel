import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  View,
  ScrollView,
  Animated,
  Image,
  Dimensions,
  Platform,
  TouchableOpacity,
  PermissionsAndroid,
  Modal,Alert,ImageBackground,
  BackHandler,
    DeviceEventEmitter,
    AsyncStorage
} from 'react-native';
// import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box'
import { connect } from 'react-redux';
// import LocationServicesDialogBox from 'react-native-android-location-services-dialog-box';
import ModalFilter from '../modal/filter.modal.component/filter.modal.component';
import Icon from 'react-native-vector-icons/FontAwesome5'
import {fetchMerchant, dataFromMaps} from '../../actions/index';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
import MapView, { Circle,Marker } from "react-native-maps";
import Images from '@assets/images'
import gambar from '../../assets/img/food_stall-icon.android.png'
const { width, height } = Dimensions.get("window");

const CARD_HEIGHT = height / 4;
const CARD_WIDTH = CARD_HEIGHT - 50;

type Props = {};
class MapsHome extends Component<Props> {

  constructor(props) {
    super(props);
    this.state = {
      initialRender: true,
      indicatorModalFilter:false,
      marginBottom:1,
      mapRegion: null,
      lastLong: null,
      lastLat: null,
      markers:[],
      ready: true,
      filteredMarkers: [],
      region: {},
      initialPosition: null,
    }
  }
 
  componentWillMount(){
    
    navigator.geolocation.getCurrentPosition(
      (position) => {
        if(position === null){
          // alert('kasdkn')
        }else{
          this.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          });

        }
      },
      {
        enableHighAccuracy: true,
        maximumAge: 100,
        
      }
    );
    this.index = 0;
    this.animation = new Animated.Value(0);
  }

  componentDidMount(){
    this.watchID = navigator.geolocation.watchPosition((position) => {
      // Create the object to update this.state.mapRegion through the onRegionChange function
      if(position === null){
      } else {
        let region = {
          latitude:       position.coords.latitude,
          longitude:      position.coords.longitude,
          latitudeDelta:  0.005,
          longitudeDelta: 0.005
        }

        let object = {
          long:region.longitude,
          lat:region.latitude
        }
        AsyncStorage.setItem('LONG_LAT',JSON.stringify(object))
        this.onRegionChange(region, region.latitude, region.longitude);
        this.props.fetchMerchant(region.latitude, region.longitude
        )
      }
    });
  }

dataMerchantToBuyFoodPage(value){
  // alert(JSON.stringify(value))
this.props.dataFromMaps(value)
}
_onMapReady() {
  PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
  .then(granted => {
    this.setState({ marginBottom: 0 });
    console.log(granted)
  });
}
  onRegionChange(region, lastLong, lastLat) {
    // alert(lastLong)
    this.setState({
      mapRegion: region,
      // If there are no new values set use the the current ones
      lastLat: lastLat || this.state.lastLat,
      lastLong: lastLong || this.state.lastLong,
    }, 
    );
  }
 async requestLocationPermission() 
{
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        'title': 'Digitalz',
        'message': 'Example App access to your location '
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      // console.log("You can use the location")
      alert("You can use the location");
    } else {
      // console.log("location permission denied")
      alert("Location permission denied");
    }
  } catch (err) {
    console.warn(err)
  }
}

  componentWillReceiveProps(nextProps){//gunanya untuk ngambil data dari belakang
    if(nextProps.merchantData.result){
      // alert(JSON.stringify(nextProps))
      let tampung = []
      for (let i = 0; i < nextProps.merchantData.result.length; i++) {
        let object ={ 
          coordinate: {
            longitude: Number(nextProps.merchantData.result[i].long),
            latitude: Number(nextProps.merchantData.result[i].lat),
            name: nextProps.merchantData.result[i].name,
            id: nextProps.merchantData.result[i].id,
            desc:nextProps.merchantData.result[i].desc,
          },
          
        }
        tampung.push(object)
      }
      this.setState({markers: tampung})
      this.props.dataFromMaps(tampung)
    }
  }
  componentWillUnmount(){
      this.props.fetchMerchant(
        this.state.lastLong,this.state.lastLat
      )
    
    navigator.geolocation.clearWatch(this.watchID);
  }

  static navigationOptions = {
    tabBarLabel: 'Favorit',    
  };
  openModalFilter() {
    this.setState({indicatorModalFilter: !this.state.indicatorModalFilter})
  }
  render() {
    const interpolations = this.state.markers.map((marker, index) => {
      const inputRange = [
        (index - 1) * CARD_WIDTH,
        index * CARD_WIDTH,
        ((index + 1) * CARD_WIDTH),
      ];
      const scale = this.animation.interpolate({
        inputRange,
        outputRange: [1, 2.5, 1],
        extrapolate: "clamp",
      });
      const opacity = this.animation.interpolate({
        inputRange,
        outputRange: [0.35, 1, 0.35],
        extrapolate: "clamp",
      });
      return { scale, opacity };
    });
    return(
      <View style={styles.container}>
        <MapView
          ref={map => this.map = map}
          initialRegion={this.state.mapRegion}
          showsMyLocationButton={true}
          showsUserLocation={true}
          followsUserLocation={true}
          showsScale={true}
          onMapReady={this._onMapReady}
          style={styles.map}
          >
          {this.state.markers.map((marker, index) => {
            
            const scaleStyle = {
              transform: [
                {
                  scale: interpolations[index].scale,
                },
              ],
            };
            const opacityStyle = {
              opacity: interpolations[index].opacity,
            };
            return (
              <MapView.Marker key={index} coordinate={marker.coordinate} 
              image={gambar}
              >
                <Animated.View style={[styles.markerWrap, opacityStyle]}>
                </Animated.View>
              </MapView.Marker>
            );
          })}
        </MapView>
        <View style={{flexDirection:'column', position: 'absolute', alignContent:'flex-end', backgroundColor: 'transparent', alignSelf: 'flex-end', top: '8.5%', right: '3%',paddingBottom:10,height:50}}>
            <View style={{backgroundColor:'transparent',flex:1,justifyContent:'center',alignItems:'center',width:38,borderColor:'black',borderRadius:50}}>
              <TouchableOpacity onPress={()=> {this.openModalFilter()}} >
                <Image 
                  source={Images.filterIcon}
                  style={{width:width/11,height:width/11}}
                  />
              </TouchableOpacity>
            </View>
        </View>
        <View style={{flexDirection:'column', position: 'absolute', alignContent:'flex-end', backgroundColor: 'transparent', alignSelf: 'flex-end',top:'15%', right: '3%',height:40}}>
            <View style={{flex:1,backgroundColor:'transparent',justifyContent:'center',alignItems:'center',width:38,borderColor:'black',borderRadius:50}}>
              <TouchableOpacity>
                 <Image 
                  source={Images.searchIcon}
                  style={{width:width/11,height:width/11}}
                  />
              </TouchableOpacity>
            </View>
        </View>            
        <Animated.ScrollView
          horizontal
          scrollEventThrottle={1}
          showsHorizontalScrollIndicator={false}
          snapToInterval={CARD_WIDTH}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {
                    x: this.animation,
                  },
                },
              },
            ],
            { useNativeDriver: true }
          )}
          style={styles.scrollView}
          contentContainerStyle={styles.endPadding}
        >         
        </Animated.ScrollView>
        <Modal
          visible={this.state.indicatorModalFilter}
          animationType={'fade'}
          transparent={false}
          onRequestClose={() => this.openModalFilter()}
          >
            <View style={{
            flex: 1,
            // flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center'}}>
                <View style={{
              width: width,
              height: height}}>
            <ModalFilter openModalFilter={() => this.openModalFilter()}/>
            </View>
          </View>
        </Modal>        
      </View>      
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
  },
  scrollView: {
    position: "absolute",
    bottom: 30,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH,
  },
  textContent: {
    flex: 1,
  },
  cardtitle: {
    fontSize: 12,
    marginTop: 5,
    fontWeight: "bold",
  },
  cardDescription: {
    fontSize: 12,
    color: "#444",
  },
  markerWrap: {
    alignItems: "center",
    justifyContent: "center",
  },
  marker: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: "rgba(130,4,150, 0.9)",
  },
  ring: {
    width: 24,
    height: 24,
    borderRadius: 12,
    backgroundColor: "rgba(130,4,150, 0.3)",
    position: "absolute",
    borderWidth: 1,
    borderColor: "rgba(130,4,150, 0.5)",
  },
  bubble:{
    flex:1,   
    borderRadius: 20,
    position:'absolute',
    backgroundColor:'blue'
  },
  searchInput:{
    flex:1,
    width:width/1.5,
    justifyContent:'center',    
    paddingVertical: 50,
    backgroundColor:'red'
  },
  map:{
    width:width,
    height:height/1.21,    
  }
});

const mapStateToProps = (state) => {
  return {
    merchantData: state.filterData.merchantData,
    dataFromMaps: state.filterData.dataFromMaps
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMerchant: (long,lat) => { dispatch( fetchMerchant(long,lat) ) },
    dataFromMaps : (value) => { dispatch (dataFromMaps(value))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MapsHome)
