import React, { Component } from 'react';
import {View,
  Text,
  Modal,
  StyleSheet,
  Dimensions,
  Slider,
  TouchableOpacity,
  Image
} from 'react-native';
import Images from '@assets/images';
import Icon from 'react-native-vector-icons/FontAwesome5';
var { height,width } = Dimensions.get('window');
class RadioButton extends Component
{
    constructor(){
      super();
    } 
    render(){
      return(
        <TouchableOpacity onPress = { this.props.onClick } activeOpacity = { 0.8 } style = { styles.radioButton }>
          <View style = {[ styles.radioButtonHolder, { height: this.props.button.size, width: this.props.button.size, borderColor: this.props.button.color }]}>
          {
            (this.props.button.selected)
            ?
            (<View style = {[ styles.radioIcon, { height: this.props.button.size / 2, width: this.props.button.size / 2, backgroundColor: this.props.button.color }]}></View>)
            :
            null
            }
            </View>
          <Text style = {[ styles.label, { color: this.props.button.color }]}>{ this.props.button.label }</Text>
        </TouchableOpacity>
        
        );
    }
}
const MODAL_HEIGHT= height/3;
const MODAL_WIDTH = MODAL_HEIGHT -20;
export default class App extends Component<{}>{
  constructor(){
    super();
      this.state = { 
        radioItems: [{
        label: <Image source={Images.tcashIcon}style={{height:60,width:180}}/>,
        size: 30,
        color: '#fb4a6f',
        selected: false,
        value: 'tcash'
    },{
        label: 'Tunai',
        size: 30,
        color: '#fb4a6f',
        selected: false
        }, {
          label: 'GoPay',
          size: 30,
          color: '#fb4a6f',
          selected: false,
          value:'tunai'
      },             
      ], selectedItem: '' }
  }
 
    componentDidMount()
    {
        this.state.radioItems.map(( item ) =>
        {
            if( item.selected == true )
            {
                this.setState({ selectedItem: item.value });
            }
        });
    }
 
    changeActiveRadioButton(index){
        this.state.radioItems.map(( item ) =>{
            item.selected = false;
        });
        this.state.radioItems[index].selected = true;
        this.setState({ radioItems: this.state.radioItems }, () =>{
          this.setState({ selectedItem: this.state.radioItems[index].label });
        });
    }
    getVal(val){
    
    } 
 
  render(){
    return(
    <View style={styles.container}>
      <View style={styles.header}>        
        <View style={{flexDirection:"row",flex:1,}}>
          <View style={{width:40}}>
            <View style={{flex:1,alignItems:'center',justifyContent:'center',}}>                  
              <TouchableOpacity onPress={()=>{this.goBacktoFoodChoose()}}>
              <Image 
              source={Images.arrowBackIcon}
              style={{width:width/22,height:width/22,alignSelf:'center'}}                  
              />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{flex:2,alignSelf:'center',justifyContent:'flex-start',}}>
            <Text style={{fontSize:width/25,fontWeight:'bold',left:'5%',color:'#454545'}}>Beli Makanan</Text>
          </View>
        </View>
      </View>
      <View style={styles.content}>
        <View style={{borderBottomColor:'gray',borderBottomWidth:.5,}}>
          <View style={{height:height/7,justifyContent:'center',width:width/1.05,right:'4%',left:'4%'}}>
            <View style={{flexDirection:'row'}}>
              <View style={{flex:1}}>
                <Text>Jarak dari Lokasimu</Text>
              </View>
              <View style={{flex:1}}>
                <Text style={styles.rangeText}>{this.state.range}km</Text>
              </View>
            </View>
          <Slider
            style={{ width: 300 }}
            step={10}
            minimumValue={10}
            maximumValue={500}
            value={this.state.range}
            onValueChange={val => this.setState({ range: val })}
            onSlidingComplete={ val => this.getVal(val)}
            />
          </View>

        </View>
        {/* ================= Helpers filter ================ */}
        <View style={{borderBottomColor:'gray',borderBottomWidth:.5,}}>
          <View style={{height:height/7,justifyContent:'center',width:width/1.05,right:'4%',left:'4%'}}>
            <Text>Helpers</Text>
            <View style={{flexDirection:'row',}}>
              {/* <View style={{flex:1}}> */}
                <TouchableOpacity>
                  <View style={styles.inerViewButton}>
                    <View style={styles.subInerViewButton}>
                        <Text style = {styles.textButtonEdit}>Verified</Text>
                    </View>
                  </View>
                </TouchableOpacity>

              {/* </View> */}
              {/* <View style={{flex:1}}> */}
                <TouchableOpacity>
                  <View style={styles.inerViewButton}>
                    <View style={styles.subInerViewButton}>
                        <Text style = {styles.textButtonEdit}>All</Text>
                    </View>
                  </View>
                </TouchableOpacity>

              {/* </View> */}
            </View>
          </View>
        </View>
        {/*====================  helpers Rating====================== */}
        <View  style={{borderBottomColor:'gray',borderBottomWidth:.5,}}>
          <View style={{height:height/7,justifyContent:'center',width:width/1.05,right:'4%',left:'4%'}}>

          <Text>Helpers Rating</Text>
          <View style={{flexDirection:'row',}}>
            
              <TouchableOpacity>
                <View style={styles.inerViewButton}>
                  <View style={styles.subInerViewButton}>
                      <Text style = {styles.textButtonEdit}>Bronze</Text>
                  </View>
                </View>
              </TouchableOpacity>
            
            
              <TouchableOpacity>
                <View style={styles.inerViewButton}>
                  <View style={styles.subInerViewButton}>
                      <Text style = {styles.textButtonEdit}>Gold</Text>
                  </View>
                </View>
              </TouchableOpacity>
            
            
              <TouchableOpacity>
                <View style={styles.inerViewButton}>
                  <View style={styles.subInerViewButton}>
                      <Text style = {styles.textButtonEdit}>Silver</Text>
                  </View>
                </View>
              </TouchableOpacity>
            
            
              <TouchableOpacity>
                <View style={styles.inerViewButton}>
                  <View style={styles.subInerViewButton}>
                      <Text style = {styles.textButtonEdit}>All</Text>
                  </View>
                </View>
              </TouchableOpacity>
            
            </View>
          </View>

        </View>

          {/* ============================Merchant ======================== */}
          <View style={{borderBottomColor:'gray',borderBottomWidth:.5,}}>

            <View style={{height:height/7,justifyContent:'center',width:width/1.05,right:'4%',left:'4%'}}>

              <View style={{flexDirection:'row',paddingBottom:height/50}}>
                <View style={{flex:1}}>
                  <Text>Merchant</Text>
                </View>
                <View style={{flex:1,color:'#fb4a6f'}}>
                  <Text style={{textAlign:'justify'}}>lihat semua</Text>
                </View>
              </View>
              <View style={{flexDirection:'row'}}>
                
                  <TouchableOpacity>
                    <View style={styles.inerViewButton}>
                      <View style={styles.subInerViewButton}>
                          <Text style = {styles.textButtonEdit}>Makanan</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
             
                
                  <TouchableOpacity>
                    <View style={styles.inerViewButton}>
                      <View style={styles.subInerViewButton}>
                          <Text style = {styles.textButtonEdit}>Sayur</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
           
                
                  <TouchableOpacity>
                    <View style={styles.inerViewButton}>
                      <View style={styles.subInerViewButton}>
                          <Text style = {styles.textButtonEdit}>Fast Food</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
           
                
                  <TouchableOpacity>
                    <View style={styles.inerViewButton}>
                      <View style={styles.subInerViewButton}>
                          <Text style = {styles.textButtonEdit}>All</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
              
              </View>
            </View>
          </View>

          {/* ================Merchant Rating============== */}
          <View style={{borderBottomColor:'gray',borderBottomWidth:.5,}}>

            <View style={{height:height/7,justifyContent:'center',width:width/1.05,right:'4%',left:'4%'}}>
              <View style={{flexDirection:'row'}}>
                <View style={{flex:1}}>
                  <Text>Merchant Rating</Text>
                </View>
                <View style={{flex:1}}>
                  <Text>lihat semua</Text>
                </View>
                </View>
                  <View style={{flexDirection:'row'}}>
                    
                      <TouchableOpacity>
                        <View style={styles.inerViewButton}>
                          <View style={styles.subInerViewButton}>
                              <Text style = {styles.textButtonEdit}>Gold</Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                      <TouchableOpacity>
                        <View style={styles.inerViewButton}>
                          <View style={styles.subInerViewButton}>
                              <Text style = {styles.textButtonEdit}>Silver</Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                      <TouchableOpacity>
                        <View style={styles.inerViewButton}>
                          <View style={styles.subInerViewButton}>
                              <Text style = {styles.textButtonEdit}>Bronze</Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                      <TouchableOpacity>
                        <View style={styles.inerViewButton}>
                          <View style={styles.subInerViewButton}>
                            <Text style = {styles.textButtonEdit}>All</Text>
                          </View>
                        </View>
                      </TouchableOpacity>

                  </View>
            </View>
          </View>
          </View>
          <View style={{height:height/15,width:width,backgroundColor:'#ffffff',bottom:0,position:'absolute'}}>
            <View style={{position:'relative',height:30,backgroundColor:'#fb4a6f',width:width/1.2,alignSelf:'center',borderRadius:5,margin:10,justifyContent:'center'}}>
              <TouchableOpacity onPress={()=>{this.goToPaymentOrdered()}}>
                <Text style={{textAlign:'center',color:'#fff'}}>Pesan</Text>
              </TouchableOpacity>
            </View>
          </View>
      </View>
    );
  }
}
 
const styles = StyleSheet.create(
{
  container: {
    flex: 1,
    flexDirection:'column',
    backgroundColor:'#ffffff'
  },
  header: {
    backgroundColor:'#ffffff',
    height:height/15,    
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,

   // its for android 
    elevation: 5,
    position:'relative',
  },
  content:{
    // height:÷height/1,
    backgroundColor:'#ffffff',
    flex:10,
    flexDirection:'column'
  },
  footer:{
    flex:1
  },
  inerViewButton: {
    paddingRight:width/30,
    // flex:1,
    alignItems:'center',
    justifyContent:'center',
  },
  subInerViewButton: {
    borderRadius:5,
    width:width/5,
    padding:5,
    // paddingLeft:90,
    // paddingRight:90,
    color:'white',
    backgroundColor:'#fb4a6f',
    alignContent:'center',
    alignItems:'center',
  },
  
});