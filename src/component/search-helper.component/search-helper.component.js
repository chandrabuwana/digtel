import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux'
import Images from '@assets/images';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
type Props = {};
class SearchHelper extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      dataHelper:[],
      dataOrderan:[]
    }
  }
  componentWillMount(){
  }
  componentDidMount(){
    // alert(JSON.stringify(this.props.dataHelperPage))
    
  //   alert(JSON.stringify(this.props.dataHelperPage))
    // alert(JSON.stringify(this.props.dataPlaceOrder))
  }
  componentWillReceiveProps(nextProps){
  // alert(JSON.stringify(nextProps.dataPlaceOrder))
    this.setState({dataHelper:nextProps.dataHelperPage})
    this.setState({dataOrderan:nextProps.dataPlaceOrder})
  }
  goToHelperAccept(){
    // alert('hola')
    this.props.navigation.navigate('HelperAccept')
  }
  goToPaymentOrdered(){
    this.props.navigation.navigate('PaymentOrdered')
  }
  render() {
    // alert(JSON.stringify(this.state.dataOrderan))
    return(
      <View style={styles.container}>
        <View style={styles.header}>        
          <View style={{flexDirection:"row",flex:1,}}>
            <View style={{width:40}}>
              <View style={{flex:1,alignItems:'center',justifyContent:'center',}}>
                <TouchableOpacity onPress={()=>{this.goToPaymentOrdered()}}>
                <Image 
                  source={Images.closeIcon}
                 
                  size={10}
                />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{flex:2,alignSelf:'center',justifyContent:'flex-start',}}>
              
            </View>
          </View>
        </View>
        <View>
          <Text style={{textAlign:"center",fontSize:width/15,fontWeight:"bold",color:'#4a4a4a'}}>Mencari helper </Text>
          <Text style={{textAlign:"center",fontSize:width/15,fontWeight:"bold",color:'#4a4a4a'}}>di dekatmu</Text>
          <View style={{height:height/2}}>
            <Text style={{textAlign:"center",fontSize:width/10,fontWeight:"bold",color:'#4a4a4a'}}>Ceritanya besok ada loadingnya disini kalau sudah bisa SVG hehehe yoshaaa....</Text>
          </View>
          <View style={{height:height/6,flexDirection:'row',justifyContent:'center'}}>
            <View style={{flex:1,marginRight:width/20,}}>
            <Image 
                  source={Images.groupIcon}
                  style={{alignSelf:'flex-end',justifyContent:'center',height:height/9,width:height/9 }}
                  // size={height/7}
                />
            </View>
            <View style={{flex:1,}}>
              <Text style={{fontWeight:'bold',}}>Beli makanan</Text>
              
              <Text style={{fontWeight:'bold',}}>{this.state.dataHelper.name}</Text>
              <Text style={{ }}>{this.state.dataOrderan.quantity} items</Text>
              <Text style={{}}>{this.state.dataOrderan.price}</Text>
            </View>
          </View>
        </View>
        <View style={styles.footerView1}>
          <View style={styles.footerView2}>
            <TouchableOpacity onPress={()=>{this.goToHelperAccept()}}>
              <Text style={{textAlign:'center',color:'#fff',fontWeight:'bold'}}>Batalkan Order</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'column',
    backgroundColor:'#ffffff'
  },
  header: {
    backgroundColor:'#ffffff',
    height:height/15,
    position:'relative',
  },
  footerView1:{
    height:height/11.5,
    width:width,
    backgroundColor:'#ffffff',
    bottom:0,
    position:'absolute',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.6,
    elevation: 5,
  },
  footerView2:{
    position:'relative',
    height:30,
    backgroundColor:'#fb4a6f',
    width:width/1.2,
    alignSelf:'center',
    borderRadius:5,
    margin:10,
    justifyContent:'center'
  }
});

const mapStateToProps = (state) => {
  return {
    dataPlaceOrder:state.filterData.dataPlaceOrder,
    dataHelperPage:state.filterData.dataHelperPage
    // indicatorLogin: state.filterData.indicatorLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchHelper)
