import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  TouchableNativeFeedback,
  Animated,
  ScrollView,
  Image,
} from 'react-native';
import { connect } from 'react-redux';
import FoodCategory from './category-food.component'
import {fetchProductFromMerchant,fetchProductFromMerchantDup,dataAfterChooseFood, fetchAgainProductFromMerchant} from '../../actions/index'
import Images from '@assets/images';
import FoodItem from './food-item.component'
import SlidingUpPanel from 'rn-sliding-up-panel';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import formatRupiah from '@helpers/IDRFormat'
import Icon from 'react-native-vector-icons/FontAwesome5';
import {BoxShadow} from 'react-native-shadow';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');

var box_count = 3;
var box_height = height / box_count;
type Props = {};
class FoodChoose extends Component<Props> {
  static defaultProps = {
    draggableRange: {
      //untuk ngatur naik turunnya bottom sheet
      top: height,
      bottom: 450
    }
  }
  _draggedValue = new Animated.Value(-900)
  constructor(props) {
    super(props);
    this.state = {
      cartBelanja: null,
      product:'',
      counter:0,
      totalCounter:0,
      cartShopAfterChoose:[]
    }
  }
  componentWillMount(){
    this.setState({cartBelanja: this.props.dataChoosen})
  }
  componentWillUnmount(){
    // this.props.fetchProductFromMerchantDup()
  }
  componentWillReceiveProps(nextProps) {
    this.setState({cartBelanja: nextProps.dataChoosen})
  }
  goBackToBuyFood(){
    this.props.navigation.navigate('BuyFood')
  }
  goToPaymentOrdered(data){
    this.state.cartShopAfterChoose
    this.props.navigation.navigate('PaymentOrdered')
  }

  tambahBelanja (val){
    let tamp =[]
    let idBelanja = val.id
    let dataAwal = this.state.cartBelanja
    let dataIndex = dataAwal.findIndex((dataIndexs) => {
      return dataIndexs.id === idBelanja
    })
    dataAwal.splice(dataIndex, 1, val)
    this.props.fetchAgainProductFromMerchant(dataAwal)
    this.maksaAnjing(dataAwal)
  }

  hapusBarang(val){
    if(val.qwt === 0){
    }else{
      val.qwt -=1
      let idBelanja = val.id
      let dataAwal = this.state.cartBelanja
      let dataIndex = dataAwal.findIndex((dataIndexs) => {
        return dataIndexs.id === idBelanja
      })
      dataAwal.splice(dataIndex, 1, val)
      this.maksaAnjing(dataAwal)
      this.props.fetchAgainProductFromMerchant(dataAwal)
    }
  }

  maksaAnjing(value) {
    this.setState({cartBelanja: value})
  }

  render() {
    const shadowOpt = {
      width:160,
      height:170,
      color:"#000",
      border:2,
      radius:3,
      opacity:0.2,
      x:0,
      y:3,
      style:{marginVertical:5}
    }
    return(
      <View style={styles.container}>
        <View style={styles.view2}>
          <View style={{flexDirection:"row",flex:1}}>
            <View style={{width:40}}>
              <View style={{flex:1,alignItems:'center',justifyContent:'center',}}>
                <TouchableOpacity onPress={()=>{this.goBackToBuyFood()}}>
                <Image
                  source={Images.arrowBackIcon}
                  style={{width:width/22,height:width/22,alignSelf:'center'}}
                />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{flex:2,alignSelf:'center',justifyContent:'flex-start',}}>
              <Text style={{fontSize:width/25,fontWeight:'bold',left:'5%',color:'#454545'}}>Pedagang di dekatmu</Text>
            </View>
            <View style={{flex:1,justifyContent:'flex-end',alignItems:'flex-end'}}>
              <View style={{right:'10%',bottom:'25%'}}>
                <TouchableOpacity onPress={()=>{this.goBackToBeranda()}}>
                <Image
                  source={Images.zoomIcon}
                  style={{width:width/15,height:width/15,alignSelf:'center'}} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      <ParallaxScrollView
          backgroundColor="white"
          contentBackgroundColor="pink"
          parallaxHeaderHeight={height/3.5}
          renderForeground={() => (
          <View style={{height:height/3.5,}}>
            <Image
              style={{width:'100%',height:'100%'}}
              source={{uri:this.props.merchantDataChoosen.desc}} />
          </View>
          )}>
          <View style={styles.panel}>
            <View style={{flex:1}}>
              <View style={[styles.boxShadow,shadow]}>
                <Text style={{fontSize:20,left:'4%',color:'#4a4a4a',fontWeight:'bold'}}>{this.props.merchantDataChoosen.name}</Text>
                <Text style={{paddingBottom:10,left:'4%'}}>Makanan</Text>
                <View style={styles.containerImage}>
                <View style={{flex:1,flexDirection:'row',position:'absolute',bottom:'5%',left:'4%'}}>
                <View style={{flexDirection:'row',flex:2,left:'4%',}}>
                  <View style={{flex:2,borderRadius:5,height:height/25,width:width/3,borderColor:'##d0cece',borderWidth:0.5,backgroundColor:'white',position:'relative',justifyContent:'center',marginBottom:20}}>
                    <Text style={{textAlign:'center',color:'#454545'}}> Gold Merchant</Text>
                  </View>
                  <View style={{position:'absolute'}}>
                    <Image
                      source={Images.goldMedalIcon}
                      style={{width:width/15,height:width/11.5,alignSelf:'center',position:'absolute',bottom:-30,left:-15}} />
                  </View>
                </View>
                  <View style={{flexDirection:'row',flex:2,left:'10%'}}>
                    <View style={{flex:1,borderRadius:5,height:height/25,width:width/8,justifyContent:'center',backgroundColor:'white',borderColor:'##d0cece',borderWidth:0.5,position:'absolute'}}>
                      <Text style={{textAlign:'center'}}>50m</Text>
                    </View>
                    <Image
                      source={Images.placeholderIcon}
                      style={{width:width/16.8235294118,height:width/13,alignSelf:'center',position:'absolute',bottom:20,left:-15,}} />
                  </View>
                  <View style={{flex:2,paddingBottom:10,}}>
                    <View style={{backgroundColor:'#fb4a6f',height:height/25,borderRadius:5,width:width/8,justifyContent:'center',left:'42%'}}>
                      <Text style={{color:'white',textAlign:'center',fontSize:width/30}}>Buka</Text>
                    </View>
                  </View>
                </View>
              </View>
              </View>
              <View style={{flexDirection:'row',height:height/12,paddingTop:10,}}>
                <View style={{flex:6,}}>
                  <FoodCategory/>
                </View>
              </View>
              <View style={{flex:4,marginRight:height/60,marginLeft:height/60}}>
                <ScrollView >
                  {this.state.cartBelanja ? this.state.cartBelanja.map((data,index) => {
                    return(
                  <FoodItem datas={data}
                  tambahBelanja={(val) => this.tambahBelanja(val)}
                  hapusBarang={(val) => this.hapusBarang(val)} />
                    )
                  }):null}
                </ScrollView>
              </View>
            </View>
            <View style={{width:width,height:height/16,backgroundColor:'transparent'}}>
              <View style={styles.floatingButtonCancelOrder}>
                <TouchableOpacity onPress={()=> {this.goToPaymentOrdered()}}>
                  <Text style={{color:'#ffffff'}}>Atau pesan manual</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
      </ParallaxScrollView>
        <View style={styles.footerView1}>
          <View style={styles.footerView2}>
            <TouchableOpacity onPress={()=>{this.goToPaymentOrdered()}}>
              <Text style={{textAlign:'center',color:'#fff'}}>Pesan</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const shadow = {
  shadowColor: '#30C1DD',
  shadowRadius: 10,
  shadowOpacity: 0.6,
  elevation: 4,
  shadowOffset: {
    width: 0,
    height: 4
  }
}

const styles = StyleSheet.create({
  view2:{
    height:height/15,
    backgroundColor:'#ffffff',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,},
  boxShadow: {
    paddingTop:10,
    height:height/6,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
  },
  panel:{
    flex: 1,
    backgroundColor: '#ffffff',
    position: 'relative',
    paddingBottom:height/8
  },
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  footerView1:{
    height:height/11.5,
    width:width,
    backgroundColor:'#ffffff',
    bottom:0,
    position:'absolute',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 5,
  },
  footerView2:{
    position:'relative',
    height:30,
    backgroundColor:'#fb4a6f',
    width:width/1.2,
    alignSelf:'center',
    borderRadius:5,
    margin:10,
    justifyContent:'center'
  },
  box: {
    height: box_height
  },
  box1: {
    flex:1,
    backgroundColor: '#ffffff',
    shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,

      elevation: 3,
  },
  box2: {
    flexDirection:'column',
    flex:10,
    backgroundColor: '#fff'
  },
  box3: {
    flex:1,
    backgroundColor: '#ffffff',
    shadowRadius: 4,
  shadowOffset: {
    width: 2,
    height: -3,
  },
  shadowOpacity: 0.25,
  shadowColor: '#000000',
  elevation: 5,
  },
  containerImage:{
    flex:1,
    paddingTop:height/30,
    justifyContent:'center',
    alignItems:'center',
  },
  floatingButtonCancelOrder:{
    backgroundColor:'#fb4a6f',
    borderRadius:5,
    height:height/18,
    width:width/2.5,
    position:'absolute',
    right:'5%',
    top:'1%',
    // bottom:'4%',
    justifyContent:'center',
    alignItems:'center',
    flex:3}
});

const mapStateToProps = (state) => {
  return {
    merchantData: state.filterData.merchantData,
    merchantDataChoosen: state.filterData.merchantDataChoosen,
    dataChoosen: state.filterData.dataChoosen,
    dataFromBuyFood: state.filterData.dataFromBuyFood
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchProductFromMerchant: (data) => { dispatch( fetchProductFromMerchant(data) ) },
    fetchProductFromMerchantDup: (data) => { dispatch( fetchProductFromMerchantDup(data) ) },
    dataAfterChooseFood: (data) => { dispatch( dataAfterChooseFood(data) ) },
    fetchAgainProductFromMerchant: (data) => { dispatch( fetchAgainProductFromMerchant(data) ) },

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FoodChoose)
