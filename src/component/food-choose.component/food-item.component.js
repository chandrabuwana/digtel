import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux'
import Images from '@assets/images';
// import {Stepper} from 'react-native-stepper';
import formatRupiah from '@helpers/IDRFormat'
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
type Props = {};

class FoodItem extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      counter:0,
      tampungPembelian: 0,
      find: '',
      totalPembelian: '',
      uangKonsumen: '',
      hitungKembalian: '',
      
    }
  }
  static getDerivedStateFromProps(props, state) {
    }
    componentDidMount(){
      
    }
  componentWillMount(){
    
    // alert(this.props.dataChoosen)
  }
  componentWillReceiveProps(nextProps) {
  }
  inputQwt(dataSatu, dataDua) {
    let penyaringan = this.state.tampungPembelian.findIndex((test) => {
      return test.id === dataSatu.id
    })
    let dataAsal = this.state.tampungPembelian
    dataAsal[penyaringan].quantity = dataDua
    this.setState({tampungPembelian: dataAsal}, () => {
      let tampungTotal = 0
      for (var i = 0; i < this.state.tampungPembelian.length; i++) {
        let kali = Number(this.state.tampungPembelian[i].quantity)*Number(this.state.tampungPembelian[i].harga_satuan)
        tampungTotal+=kali
      }
      this.setState({totalPembelian: tampungTotal})
    })
  }
  tambahBelanja(val){
    val.qwt +=1
    this.props.tambahBelanja(val)
  }

  
  hapusBarang(val) {
    this.props.hapusBarang(val)
  }
  onChangeDurationFilter(value) {
   this.setState({value:this.state.counter})
  }

  changeStringTotal(){
    parseInt()
  }
  render() {
    
    return(
      <View>
        <View style={{
          padding:5,
          flexDirection:'row',
          flex:1,
          backgroundColor: '#ffffff',
          shadowColor: "#000",
          shadowOffset: {
            width: 10,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5,}}
        >
        <View style={{flex:1,marginRight:width/20,}}>
          <Image 
          style={{width:width/5,height:width/5,borderRadius:3}}
          source={{uri:this.props.datas.desc}}
          />
        </View>
          <View style={{flex:3,marginLeft:height/60}}>
            <Text style={{fontSize:height/45,fontWeight:'bold',color:'#4a4a4a'}}>{this.props.datas.name}</Text>
            <Text style={{color:'#4a4a4a'}}>{formatRupiah(Number(this.props.datas.price))}</Text>
          </View>
          <View style={{flex:1.5,justifyContent:'flex-end'}}>
            <View style={{flexDirection:'row'}}>
              {/* <View style={{flex:1,justifyContent:'space-between',alignItems:'center',paddingTop:10}}>
                </View> */}
                  <View style={{flex:1,justifyContent:'space-between',alignItems:'center',paddingTop:10}}>
                <TouchableOpacity onPress={()=>{this.hapusBarang(this.props.datas)}}>
                  <Image
                    source={Images.minusSymbolIcon}
                    style={{height:height/30,width:height/30}}
                  />
                </TouchableOpacity>
              </View>
                <View style={{
                  flex:1.5,
                  borderColor:'#9b9b9b',
                  height:height/30,
                  width:height/30,
                  borderRadius:3,
                  borderWidth:0.3,
                  margin:10,
                  justifyContent:'center',
                  alignContent:'center'}}>
                  <Text style={{textAlign:'center'}}>{Number(this.props.datas.qwt)}</Text>
                </View>
                <View style={{
                  flex:1,
                  justifyContent:'space-between',
                  alignItems:'center',
                  paddingTop:10}}>
                    <TouchableOpacity onPress={()=>{this.tambahBelanja(this.props.datas)}}>
                      <Image
                      source={Images.plusButtonIcon}
                      style={{height:height/30,width:height/30}}
                    />
                    </TouchableOpacity>
                  </View>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const mapStateToProps = (state) => {
  return {
    dataChoosen: state.filterData.dataChoosen
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FoodItem)
