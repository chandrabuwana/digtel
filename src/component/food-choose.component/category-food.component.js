import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux'
import Images from '@assets/images';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
type Props = {};
class CategoryFood extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      indicator:1
    }
  }
  componentWillMount(){
  }
  pressButtonGroup(data){
    this.setState({indicator: data})
  }
  render() {
    return(
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        >
        <View style={{flex:1.5,}}>
          <Text style={{textAlign:'center',fontSize:height/40,fontWeight:'bold',color:'#4a4a4a',marginLeft:width/20}}>Kategori  </Text>
        </View>
        {
          this.state.indicator === 1 ?
        <TouchableOpacity style={{height:height/20,paddingRight:5}}
        onPress={()=> this.pressButtonGroup(1)} >
          <View style={styles.onStyle}>                      
            <Text style={{color:'#4a4a4a',
                textAlign:'center'}}>Makanan</Text>
          </View>
        </TouchableOpacity>:
        <TouchableOpacity style={{height:height/20,paddingRight:5}}
        onPress={()=> this.pressButtonGroup(1)} >
          <View style={styles.offStyle}>                      
            <Text style={{color:'#4a4a4a',
                textAlign:'center'}}>Makanan</Text>
          </View>
        </TouchableOpacity>
      }
        {
          this.state.indicator === 2 ?
        <TouchableOpacity style={{height:height/20,paddingRight:5}}
        onPress={()=> this.pressButtonGroup(2)} >
          <View style={styles.onStyle}>                      
            <Text style={{color:'#4a4a4a',
                textAlign:'center'}}>Minuman </Text>
          </View>
        </TouchableOpacity>:
        <TouchableOpacity style={{height:height/20,paddingRight:5}}
        onPress={()=> this.pressButtonGroup(2)} >
          <View style={styles.offStyle}>                      
            <Text style={{color:'#4a4a4a',
                textAlign:'center'}}>Minuman </Text>
          </View>
        </TouchableOpacity>
      }
      {
          this.state.indicator === 3 ?
        <TouchableOpacity style={{height:height/20,paddingRight:5}}
        onPress={()=> this.pressButtonGroup(3)} >
          <View style={styles.onStyle}>                      
            <Text style={{color:'#4a4a4a',
                textAlign:'center'}}>Obat</Text>
          </View>
        </TouchableOpacity>:
        <TouchableOpacity style={{height:height/20,paddingRight:5}}
        onPress={()=> this.pressButtonGroup(3)} >
          <View style={styles.offStyle}>                      
            <Text style={{color:'#4a4a4a',
                textAlign:'center'}}>Obat</Text>
          </View>
        </TouchableOpacity>
      }
      {
          this.state.indicator === 4 ?
        <TouchableOpacity style={{height:height/20,paddingRight:5}}
        onPress={()=> this.pressButtonGroup(4)} >
          <View style={styles.onStyle}>                      
            <Text style={{color:'#4a4a4a',
                textAlign:'center'}}>Camilan</Text>
          </View>
        </TouchableOpacity>:
        <TouchableOpacity style={{height:height/20,paddingRight:5}}
        onPress={()=> this.pressButtonGroup(4)} >
          <View style={styles.offStyle}>                      
            <Text style={{color:'#4a4a4a',
                textAlign:'center'}}>Camilan</Text>
          </View>
        </TouchableOpacity>
      }
      {
          this.state.indicator === 5 ?
        <TouchableOpacity style={{height:height/20,paddingRight:5}}
        onPress={()=> this.pressButtonGroup(5)} >
          <View style={styles.onStyle}>                      
            <Text style={{color:'#4a4a4a',
                textAlign:'center'}}>Paku</Text>
          </View>
        </TouchableOpacity>:
        <TouchableOpacity style={{height:height/20,paddingRight:5}}
        onPress={()=> this.pressButtonGroup(5)} >
          <View style={styles.offStyle}>                      
            <Text style={{color:'#4a4a4a',
                textAlign:'center'}}>Paku</Text>
          </View>
        </TouchableOpacity>
      }
      {
          this.state.indicator === 6 ?
        <TouchableOpacity style={{height:height/20,paddingRight:5}}
        onPress={()=> this.pressButtonGroup(6)} >
          <View style={styles.onStyle}>                      
            <Text style={{color:'#4a4a4a',
                textAlign:'center'}}>Kayu</Text>
          </View>
        </TouchableOpacity>:
        <TouchableOpacity style={{height:height/20,paddingRight:5}}
        onPress={()=> this.pressButtonGroup(6)} >
          <View style={styles.offStyle}>                      
            <Text style={{color:'#4a4a4a',
                textAlign:'center'}}>Kayu</Text>
          </View>
        </TouchableOpacity>
      }
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  onStyle:{
    flex:1,
    backgroundColor:'#ffbfcc',
    borderRadius:5,
    width:width/5,
    borderColor:'#fb4a6f',
    borderWidth:1,
    justifyContent:'center',
  },
  offStyle:{
    flex:1,
    backgroundColor:'white',
    borderRadius:5,
    borderColor:'#4a4a4a',
    borderWidth:1,
    width:width/5,
    justifyContent:'center',
  },
});

const mapStateToProps = (state) => {
  return {
    // indicatorLogin: state.filterData.indicatorLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryFood)
