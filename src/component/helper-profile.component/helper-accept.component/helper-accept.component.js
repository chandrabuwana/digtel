import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Dimensions,
  Animated
} from 'react-native';
import { connect } from 'react-redux';
import SlidingUpPanel from 'rn-sliding-up-panel';
import MapsHome from '../../maps-home.component/maps-home.component';

import Images from '@assets/images';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
type Props = {};
class HelperAccept extends Component<Props> {
  static defaultProps = {
    draggableRange: {
      //untuk ngatur naik turunnya bottom sheet
      top: height / 1.7,
      bottom: 290
    }
  }
  _draggedValue = new Animated.Value(-300)

  constructor(props) {
    super(props);
    this.state = {
    }
  }
  componentWillMount(){
  }
  goToHelperDataProfile(){
    this.props.navigation.navigate('HelperDataProfile')
  }
  goToPaymentOrdered(){
    this.props.navigation.navigate('PaymentOrdered')
  }

  render() {
    return(
      <View style={styles.container}>
        <View style={{flex:1}}>
          <View style={{flex:1,height:height,width:width}}>
            <MapsHome/>
          </View>
        </View>
        
        <SlidingUpPanel
          visible
          startCollapsed
          showBackdrop={false}
          ref={c => this._panel = c}
          draggableRange={this.props.draggableRange}
          onDrag={v => this._draggedValue.setValue(v)}
        >
        
          <View style={styles.panel}>
          
            <View>
            <Image source={Images.rectangleIcon}
            style={{width: 40, height: 5,alignSelf:'center',marginTop:10}}
            />
            </View>
            <View style={{borderBottomColor:'gray',borderBottomWidth:1,paddingBottom:10}}>
              <View style={{height:height/8,flexDirection:'row',}}>
                <View style={{ flex:1,flexDirection:'row'}}>
                  <TouchableOpacity onPress={()=> {this.goToHelperDataProfile()}}>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Image
                    source={{uri:'https://awsimages.detik.net.id/community/media/visual/2016/11/09/3258de99-4a22-4a16-b1c2-8791eaf9c8ec.jpg?w=780&q=90'}}
                    style={{width:width/5,height:width/5,borderRadius:150/2}}
                    />
                    </View>
                  </TouchableOpacity>

                  <View style={{flex:1,flexDirection:'column'}}>
                    <View style={{flex:1,justifyContent:"center",alignItems:"center"}}>
                    <Text style={{color:'#4a4a4a',fontWeight:'bold'}}>Chelsea Islan</Text>
                    </View>
                    <View style={{flex:1,flexDirection:'row'}}>
                      <View style={{flex:1}}>
                      <Image 
                        source={Images.goldMedalIcon}
                        size={10}
                      />
                      </View>
                      <View style={{flex:2,justifyContent:"center"}}>
                        <View style={{height:height/23,borderColor:'gray',borderWidth:1,borderRadius:5,justifyContent:"center",alignItems:'center'}}>
                          <Text style={{color:'#454545'}}>Verified</Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={{ flex:1,flexDirection:'row'}}>
                  <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
                    <TouchableOpacity>
                      <View style={{height:height/10,width:height/10,borderRadius:50,borderColor:'gray',borderWidth:1,backgroundColor:'#ffffff'}}>
                      <Image 
                        source={Images.telephoneIcon}
                        size={10}
                        style={{alignSelf:'center',justifyContent:'center',marginTop:10}}
                      />
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
                    <TouchableOpacity>
                      <View style={{height:height/10,width:height/10,borderRadius:50,borderColor:'gray',borderWidth:1,backgroundColor:'#ffffff'}}>
                      <Image 
                        source={Images.chatIcon}
                        size={10}
                        style={{alignSelf:'center',justifyContent:'center',marginTop:10}}
                      />
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
            
            <View style={{height:height/25}}>
              <Text>Ringkasan Transaksi</Text>
            </View>
            
            <View style={{height:height/20,paddingBottom:5}}>
              <View style={{flex:1,flexDirection:'row',}}>
                <View style={{ flex:1,flexDirection:'row'}}>
                  <View style={{flex:1,justifyContent:'flex-start',alignItems:'flex-start'}}>
                  <Image 
                    source={Images.currencyIcon}
                    size={3}
                    style={{alignSelf:'center',justifyContent:'center'}}
                  />
                  </View>
                <View style={{flex:3,justifyContent:'center',alignItems:'flex-start',}}><Text>Subtotal</Text></View>
                </View>
                <View style={{ flex:1,flexDirection:'row'}}>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}><Text>Rp</Text></View>
                <View style={{borderColor:'#454545',borderRadius:5,borderWidth:1,justifyContent:'center',alignItems:'center',flex:3}}><Text>ex:100.000</Text></View>
                </View>
              </View>
            </View>

            <View style={{height:height/20,paddingBottom:5}}>
              <View style={{flex:1,flexDirection:'row',}}>
                <View style={{ flex:1,flexDirection:'row'}}>
                  <View style={{flex:1,justifyContent:'flex-start',alignItems:'flex-start'}}>
                  <Image 
                    source={Images.deliveryIcon}
                    size={3}
                    style={{alignSelf:'center',justifyContent:'center'}}
                  />
                  </View>
                <View style={{flex:3,justifyContent:'center',alignItems:'flex-start',}}><Text>Biaya Transport</Text></View>
                </View>
                <View style={{ flex:1,flexDirection:'row'}}>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}><Text>Rp</Text></View>
                <View style={{borderColor:'#454545',borderRadius:5,borderWidth:1,justifyContent:'center',alignItems:'center',flex:3}}><Text>ex:100.000</Text></View>
                </View>
              </View>
            </View>

            <View style={{height:height/20,paddingBottom:5}}>
              <View style={{flex:1,flexDirection:'row',}}>
                <View style={{ flex:1,flexDirection:'row'}}>
                  <View style={{flex:1,justifyContent:'flex-start',alignItems:'flex-start'}}>
                  <Image 
                    source={Images.respectIcon}
                    size={3}
                    style={{alignSelf:'center',justifyContent:'center'}}
                  />
                  </View>
                <View style={{flex:3,justifyContent:'center',alignItems:'flex-start',}}><Text>Tip Helper</Text></View>
                </View>
                <View style={{ flex:1,flexDirection:'row'}}>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}><Text>Rp</Text></View>
                <View style={{borderColor:'#454545',borderRadius:5,borderWidth:1,justifyContent:'center',alignItems:'center',flex:3}}><Text>ex:100.000</Text></View>
                </View>
              </View>
            </View>

            <View style={{height:height/20,paddingBottom:5}}>
              <View style={{flex:1,flexDirection:'row',}}>
                <View style={{ flex:1,flexDirection:'row'}}>
                  <View style={{flex:1,justifyContent:'flex-start',alignItems:'flex-start'}}>
                  <Image 
                    source={Images.guaranteeIcon}
                    size={3}
                    style={{alignSelf:'center',justifyContent:'center'}}
                  />
                  </View>
                <View style={{flex:3,justifyContent:'center',alignItems:'flex-start',}}><Text>Total</Text></View>
                </View>
                <View style={{ flex:1,flexDirection:'row'}}>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}><Text>Rp</Text></View>
                <View style={{borderColor:'#454545',borderRadius:5,borderWidth:1,justifyContent:'center',alignItems:'center',flex:3}}><Text>ex:100.000</Text></View>
                </View>
              </View>
            </View>
            
          </View>
        </SlidingUpPanel>
        <View style={{width:width,height:height/16,backgroundColor:'transparent'}}>
          <View style={styles.floatingButtonCancelOrder}>
            <TouchableOpacity onPress={()=> {this.goToPaymentOrdered()}}>
              <Text style={{color:'#ffffff'}}>Batalkan</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  panel: {
    flex: 1,
    backgroundColor: '#ffffff',
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    position: 'relative',
    paddingLeft:width/20,
    paddingRight:width/20
  },
  floatingButtonCancelOrder:{
    backgroundColor:'#fb4a6f',
    borderRadius:5,
    height:height/18,
    width:width/3,
    position:'absolute',
    right:'5%',
    top:'10%',
    bottom:'4%',
    justifyContent:'center',
    alignItems:'center',
    flex:3}
});

const mapStateToProps = (state) => {
  return {
    // indicatorLogin: state.filterData.indicatorLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HelperAccept)
