'user-strict';
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux'
import Images from '@assets/images';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
type Props = {};
class AppAndro extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  componentWillMount(){
  }

  render() {
    return(
      <View style={{flexDirection:'row',marginLeft:width/20,marginRight:width/20}}>
        <View style={{flex:4,}}>
          <Text style={{fontSize:width/19,fontWeight:'bold',color:'#4a4a4a'}}>Chelsea Islan</Text>
          <Text style={{color:'#4a4a4a'}}>Helper</Text>
          <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',height:height/15}}>
            <View style={{flex:.5,marginRight:10}}>
            <Image
              source={Images.goldMedalIcon}
              style={{marginRight:20,}}
              size={10}
            />
            </View>
            <View style={{flex:1.5,height:height/23,}}>
              <View style={styles.verifiedBox}>
                <Text style={{color:'#454545',textAlign:'center'}}>Verified</Text>
              </View>
            </View>
            <View style={{flex:2,marginRight:10,color:'#4a4a4a'}}><Text>30 Review</Text></View>
            <View style={{flex:1,backgroundColor:'yellow'}}></View>
          </View>
        </View>
        <View style={{flex:1,justifyContent:"center",alignItems:'center'}}>
          <Text style={{fontSize:width/10,color:'#fb4a6f'}}>85</Text>
          <Text style={{textAlign:'center'}}>Transaksi Berhasil</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  verifiedBox:{
    flex:1,
    justifyContent:'center',
    alignSelf:'center',
    borderWidth:1,
    borderRadius:5,
    // backgroundColor:'red',
    borderColor:'rgba(208, 206, 206,0.8)',
    width:width/7,
    marginRight:20
  }
});

const mapStateToProps = (state) => {
  return {
    // indicatorLogin: state.filterData.indicatorLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppAndro)
