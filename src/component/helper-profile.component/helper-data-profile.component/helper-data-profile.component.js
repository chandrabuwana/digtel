import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Animated,
  Dimensions,
  ProgressBar
} from 'react-native';
import { connect } from 'react-redux';
import SlidingUpPanel from 'rn-sliding-up-panel'
import Images from '@assets/images';
import * as Progress from 'react-native-progress';
import ProfileHelper from './profile-helper.component';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
type Props = {};

class HelperDataProfile extends Component<Props> {
  static defaultProps = {
    draggableRange: {
      //untuk ngatur naik turunnya bottom sheet
      top: height/1.3,
      bottom: 490
    }
  }
  _draggedValue = new Animated.Value(-500)
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  componentWillMount(){
  }
  goToChoosenHelper(){
    this.props.navigation.navigate('HelperAccept')
  }

  render() {
    const shadowOpt = {
      width:160,
      height:170,
      color:"#000",
      border:2,
      radius:3,
      opacity:0.2,
      x:0,
      y:3,
      style:{marginVertical:5}
  }
  
    return(
      <View style={styles.container}>
      <View style={styles.content}>
        <View style={{flexDirection:"row",flex:1}}>
          <View style={{width:40}}>
            <View style={{flex:1,alignItems:'center',justifyContent:'center',}}>                  
              <TouchableOpacity onPress={()=>{this.goToChoosenHelper()}}>
              <Image 
                source={Images.closeIcon}
                style={{width:width/22,height:width/22,alignSelf:'center'}}
              />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{flex:2,alignSelf:'center',justifyContent:'flex-start',}}>
            <Text style={{fontSize:width/25,fontWeight:'bold',left:'5%',color:'#454545'}}>Helper</Text>
          </View>
          <View style={{flex:1,justifyContent:'flex-end',alignItems:'flex-end'}}>
          </View>
        </View>
      </View>
      <View style={{flex:1}}>
        <View style={{backgroundColor:'blue',height:height/3.5,}}>
          <Image
            style={{width:'100%',height:'100%'}}
            source={{uri:'https://awsimages.detik.net.id/community/media/visual/2016/11/09/3258de99-4a22-4a16-b1c2-8791eaf9c8ec.jpg?w=780&q=90'}}
            />         
        </View>

        <SlidingUpPanel
          visible
          startCollapsed
          showBackdrop={false}
          ref={c => this._panel = c}
          draggableRange={this.props.draggableRange}
          onDrag={v => this._draggedValue.setValue(v)}>
          <View style={styles.panel}>
            <View style={{flex:1}}>
        <View style={[styles.boxShadow,shadow]}>
          <ProfileHelper/>
        </View>
          <View style={{flex:1,}}>
          <View style={{height:height/13,marginLeft:width/20,marginRight:width/20,marginTop:10}}>
            <Text style={{fontSize:width/30,fontWeight:'bold',paddingBottom:5}}>Keahlian Helper</Text>
            <View style={{flexDirection:'row'}}>
              <View style={styles.verifiedBox}>
                <Text style={{color:'#454545'}}>Beli Makanan</Text>
              </View>
              <View style={styles.verifiedBox}>
                <Text style={{color:'#454545'}}>Belanja</Text>
              </View>
              <View style={styles.verifiedBox}>
                <Text style={{color:'#454545'}}>Bersih-bersih</Text>
              </View>
            </View>
          </View>
          <View style={{flex:1.5,height:height/3,marginLeft:width/20,marginRight:width/20,flexDirection:'column'}}>
          <Text style={{fontSize:width/30,fontWeight:'bold',paddingBottom:5}}>Review Layanan Helper</Text>
          
          <View style={{height:height/11,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
            <View style={{flex:2,}}>
              <Image 
                  source={Images.smileIcon}
                  // size={10}
                  style={{height:width/7,width:width/7}}
                />
            </View>
            <View style={{flex:5,justifyContent:'center',}}>
            <Progress.Bar 
              progress={0.8}
              width={null}
              borderRadius={7}
              color={rgb(44, 199, 44)}
              unfilledColor={rgb(216, 216, 216)}
              borderColor={rgb(255, 255, 255)}
              height={10}/>
            </View>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:width/30,fontWeight:'bold'}}>25</Text>
            </View>
          </View>

          <View style={{height:height/11,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
            <View style={{flex:2}}>
            <Image 
                source={Images.mehIcon}
                // size={15}
                style={{height:width/7,width:width/7}}
              />
            </View>
            <View style={{flex:5,justifyContent:'center',}}>
            <Progress.Bar 
              progress={0.5}
              width={null}
              borderRadius={7}
              color={rgb(237, 191, 22)}
              unfilledColor={rgb(216, 216, 216)}
              borderColor={rgb(255, 255, 255)}
              height={10}/>
            </View>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:width/30,fontWeight:'bold'}}>25</Text>
            </View>
          </View>
          <View style={{height:height/11,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
            <View style={{flex:2}}>
            <Image 
                source={Images.sadIcon}
                // size={15}
                style={{height:width/7.5,width:width/7.5}}
              />
            </View>
            <View style={{flex:5,justifyContent:'center',}}>
            <Progress.Bar 
              progress={0.3}
              width={null}
              borderRadius={7}
              color={rgb(226, 28, 28)}
              unfilledColor={rgb(216, 216, 216)}
              borderColor={rgb(255, 255, 255)}
              height={10}/>
            </View>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:width/30,fontWeight:'bold'}}>25</Text>
            </View>
          </View>
          </View>
        </View>
        </View>
      </View>
    </SlidingUpPanel> 
        </View>
      </View>
    )
  }
}
const shadow = {  
  shadowColor: '#30C1DD',
  shadowRadius: 10,
  shadowOpacity: 0.6,
  elevation: 4,
  shadowOffset: {
    width: 0,
    height: 4
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'center',
    flexDirection:'column'
  },
  progressBarCountainer: {
    marginTop: 7,
    flexDirection:"row",
    height: 15,
    borderRadius:10
  },
  subProgressBarCountainer: {
    flex:1,
    borderColor:"#000",
    borderWidth: 2,
    borderRadius: 10
  },
  panel:{
    flex: 1,
    backgroundColor: '#ffffff',    
    position: 'relative',
    paddingBottom:height/15
  },
  boxShadow: {
    paddingTop:10,
    height:height/6,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
  },
  content:{
    top:0,
      height:height/15,
      backgroundColor:'#ffffff',
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
    },
    verifiedBox:{
      flex:1,
      justifyContent:'center',
      alignItems:'center',
      borderWidth:1,
      borderRadius:5,
      borderColor:'rgba(208, 206, 206,0.8)',
      height:height/25,
      marginRight:10
    }
});

const mapStateToProps = (state) => {
  return {
    // indicatorLogin: state.filterData.indicatorLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HelperDataProfile)
