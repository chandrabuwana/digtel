import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Dimensions,
  Animated,
  ImageBackground
} from 'react-native';
import { connect } from 'react-redux'
import Images from '@assets/images';
import LinearGradient from 'react-native-linear-gradient'
import {dataFromMaps,fetchProductFromMerchant,merchantChoosen} from '../../actions/index'
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
type Props = {};

const {height,width} = Dimensions.get('window')
const CARD_HEIGHT = height /5.9;
const CARD_WIDTH = CARD_HEIGHT;
class MenuFood extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      dataFoodMenuRecommended:[]
    }
  }
  componentDidMount(){
    this.setState({dataFoodMenuRecommended: this.props.dataFetchRecommended})
  }
  componentWillMount(){
  }
  goToOrderAfterChoose(value){
    this.props.goToOrderAfterChoose()
    this.props.merchantChoosen(value)
    this.props.fetchProductFromMerchant(value.id)
  }

  goToListFood (){
    this.props.goToListFood()
  }

  componentWillReceiveProps(nextProps){
    this.setState({dataFoodMenuRecommended: nextProps.dataFetchRecommended})
  }

  render() {
    return(
      <View style={{
        flex:2,
        }}>
        <View style={{flexDirection:'row',marginTop:width/20,marginLeft:width/20,marginRight:width/20}}>
          <View style={{flex:2,}}>
            <Text style={{color:'#454545',fontSize:height/50,marginBottom:5}}>Rekomendasi dari JunkFood</Text>
          </View>
            <View style={{flex:1,justifyContent:'flex-end',}}>
              <TouchableOpacity onPress={() => { this.goToListFood()}}>
                <Text style={{color:'#fb4a6f',textAlign:'right',fontSize:width/40,marginBottom:5}}>Lihat semua</Text>
              </TouchableOpacity>
            </View>
        </View>
        <View style={{height:height/5.3,justifyContent:'center'}}>
          <Animated.ScrollView
            horizontal
            scrollEventThrottle={1}
            showsHorizontalScrollIndicator={false}
            snapToInterval={CARD_WIDTH}
            onScroll={Animated.event(
              [{
                nativeEvent: {
                  contentOffset: {
                    x: this.animation,
                  },
                },
              },
            ],
            { useNativeDriver: true }
            )}>
            {this.state.dataFoodMenuRecommended ? this.state.dataFoodMenuRecommended.map((data, index) => {
              return(
                <TouchableOpacity onPress={()=>{this.goToOrderAfterChoose(data)}}>
                <View style={styles.card} key={index}>
                  <ImageBackground 
                  source={{uri:data.desc}}
                  style={[styles.cardImage, shadow]}
                  resizeMode="cover"
                  >
                  {/* <View style={{height:height/20,width:'100%',backgroundColor:'rgba(  0 , 0 , 0 , 0.2)',position:'absolute',bottom:0}}>
                    <Text style={{color:'#ffffff',textAlign:'center'}}>{data.name}</Text>
                  </View> */}
                  <LinearGradient colors={['rgba(  0 , 0 , 0 , 0)','rgba(  0 , 0 , 0 , 0.7)']} style={{position:'absolute',bottom:0,height:height/20,width:'100%',}}>
                    <View style={{flexDirection:'row'}}>
                      <View style={{flex:.3,margin:5,}}>
                        <Image source={Images.goldMedalIcon}
                        style={{width:width/19,height:width/15.5,left:'10%',alignSelf:'center',position:'absolute',}}/>
                      </View>
                      <View style={{flex:1,alignContent:'center',alignItems:'center',}}>
                        <Text style={{color:'#ffffff',}}>{data.name}</Text>
                      </View>
                    </View>
                 </LinearGradient>
                  </ImageBackground>
                </View>
                </TouchableOpacity>
              )
            }) : null}
          </Animated.ScrollView>
        </View>
      </View>
    )
  }
}

const shadow = {
  shadowColor: '#30C1DD',
  shadowRadius: 10,
  shadowOpacity: 0.6,
  elevation: 8,
  shadowOffset: {width: 0,height: 4}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    position: "absolute",
    left: 0,
    right: 0,
  },
  card: {
    elevation: 5,
    backgroundColor: "#FFF",
    // paddingRight: width/20,
    marginLeft: width/20,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1.0,
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    overflow: "hidden",
    borderRadius:5
  },
  cardImage: {
    flex: 1,
    width: "100%",
    height: "100%",
    alignSelf: "center",
  },

});

const mapStateToProps = (state) => {
  return {
    dataFromMapsHome: state.filterData.dataFromMapsHome,
    merchantData: state.filterData.merchantData,
    dataFetchRecommended: state.filterData.dataFetchRecommended,
    dataChoosen: state.filterData.dataChoosen,
    // indicatorLogin: state.filterData.indicatorLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // fetchMerchant: (long,lat) => { dispatch( fetchMerchant(long,lat) ) },
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
    dataFromMaps : (value) => { dispatch (dataFromMaps(value))},
    fetchProductFromMerchant : (value) => { dispatch (fetchProductFromMerchant(value))},
    merchantChoosen : (value) => { dispatch (merchantChoosen(value))},
    
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuFood)
