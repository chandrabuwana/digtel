import React, {Component} from 'react';
import {
  View,
  Text,
  Platform,
  StyleSheet,
  ScrollView,
  Row,
  Image,
  TextInput,
  Dimensions,
  Animated,
  TouchableOpacity,
  KeyboardAvoidingView,AsyncStorage
} from 'react-native';
import MapView from 'react-native-maps';
import Images from '@assets/images';
import { connect } from 'react-redux';
import MenuFoodSnack from './menu-food-snack.component'
import MenuFoodWestern from './menu-food-western.component'
import MenuFoodMakananTradisional from './menu-food-makanan-tradisional.component'
import Icon from 'react-native-vector-icons/FontAwesome5'
import {Header,Divider} from 'react-native-elements';
import InputScrollView from 'react-native-input-scroll-view';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import MenuFood from './menu-food.component';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props={}
const {height,width} = Dimensions.get('window')
const CARD_HEIGHT = height /8;
const CARD_WIDTH = CARD_HEIGHT +50;
class BuyFood extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      dataMerchant:[],
      jumlahSatuanOrder:0,
     }
  }
componentDidMount(){
}
  goToOrderAfterChoose(){
    this.props.navigation.navigate('FoodChoose')
  }
  goToListFood (){
    this.props.navigation.navigate('ListFood')
  }
  goBackToBeranda(){
    this.props.navigation.navigate('Beranda')
  }
  onLayout(e) {
    const {nativeEvent: {layout: {height}}} = e;
    this.height = height;
    this.forceUpdate();
  }

  render() { 
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={{flexDirection:"row",flex:1,}}>
            <View style={{width:40}}>
              <View style={{flex:1,alignItems:'center',justifyContent:'center',}}>                  
                <TouchableOpacity onPress={()=>{this.goBackToBeranda()}}>
                <Image 
                  source={Images.arrowBackIcon}
                  style={{width:width/22,height:width/22,alignSelf:'center'}}
                />
              </TouchableOpacity>
            </View>
          </View>
            <View style={{flex:2,alignSelf:'center',justifyContent:'flex-start',}}>
              <Text style={{color:'#454545',fontSize:width/25,fontWeight:'bold'}}>Beli Makanan</Text>
            </View>
          </View>
        </View>
        <ScrollView
        keyboardShouldPersistTaps={"always"}>
        
          <View style={styles.content}>
            <MenuFood
            goToListFood={() => this.goToListFood()}
            goToOrderAfterChoose={() => this.goToOrderAfterChoose()}
            />
            <MenuFoodSnack
            goToListFood={() => this.goToListFood()}
            goToOrderAfterChoose={() => this.goToOrderAfterChoose()}
            />
            <MenuFoodWestern
            goToListFood={() => this.goToListFood()}
            goToOrderAfterChoose={() => this.goToOrderAfterChoose()}
            />
            <MenuFoodMakananTradisional
            goToListFood={() => this.goToListFood()}
            goToOrderAfterChoose={() => this.goToOrderAfterChoose()}
            />
            
          </View>
        </ScrollView>
        <TouchableOpacity onPress={()=> {this.goToPaymentOrdered()}} style={styles.floatingButtonCancelOrder}>
          <Text style={{color:'#ffffff',}}>Atau pesan manual</Text>
        </TouchableOpacity>
        {/* Footer */}
        {/* <View style={styles.footerView1}>
          <View style={styles.footerView2}>
            <TouchableOpacity onPress={()=>{this.goToOrderAfterChoose()}}>
              <Text style={{textAlign:'center',color:'#fff'}}>Pesan</Text>
            </TouchableOpacity>
          </View>
        </View> */}
        </View>
     );
    
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'column'
  },
  header: {
    height:height/15,
    backgroundColor:'#ffffff',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  content:{
    flex:10,
    marginBottom:height/9,
    flexDirection:'column',
  },
  footerView1:{
    height:height/11.5,
    width:width,
    backgroundColor:'#ffffff',
    bottom:0,
    position:'absolute',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 5,
  },
  floatingButtonCancelOrder:{
    position: 'absolute',
    width:width/3,height:height/16,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
    borderRadius:5,
    backgroundColor:'#fb4a6f',
  },
  footerView2:{
    position:'relative',
    height:30,
    backgroundColor:'#fb4a6f',
    width:width/1.2,
    alignSelf:'center',
    borderRadius:5,
    margin:10,
    justifyContent:'center'
  },
  textHeader:{ 
    color:'#fff',
    fontWeight:'bold'    
  },
  
  endPadding: {
    // paddingRight: 20,
  },
  textContent: {
    flex: 1,
  },
  // floatingButtonCancelOrder:{
  //   backgroundColor:'#fb4a6f',
  //   borderRadius:5,
  //   height:height/18,
  //   width:width/3,
  //   position:'absolute',
  //   right:'5%',
  //   top:'1%',
  //   bottom:'4%',
  //   justifyContent:'center',
  //   alignItems:'center',
  //   flex:3
  // }
});
const mapStateToProps = (state) => {
  return {
    // indicatorLogin: state.filterData.indicatorLogin
    merchantData: state.filterData.merchantData,
    datachoosen: state.filterData.datachoosen

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMerchant: (long,lat) => { dispatch( fetchMerchant(long,lat) ) }
  }
}

export default connect(mapStateToProps,mapDispatchToProps) (BuyFood);



