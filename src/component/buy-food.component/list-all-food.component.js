import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
import {dataFromMaps} from '../../actions/index'
import Images from '@assets/images';
import Icon from 'react-native-vector-icons/FontAwesome5';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
var { height,width } = Dimensions.get('window');
const Gambar=[
  { uri:"https://d.zmtcdn.com/data/pictures/chains/8/16774318/a54deb9e4dbb79dd7c8091b30c642077_featured_v2.png"},
  { uri:"https://i1.wp.com/notepam.com/wp-content/uploads/2017/09/resep-soto-ayam-kuah-kuning.jpg?resize=680%2C486"},
  { uri:"https://d.zmtcdn.com/data/pictures/chains/8/16774318/a54deb9e4dbb79dd7c8091b30c642077_featured_v2.png"},
  { uri:"https://media.glamour.com/photos/5ab2b4ba3f4cab3d54308248/master/w_644,c_limit/starbucks-equal-pay.jpg"},
  { uri:"https://selerasa.com/wp-content/uploads/2015/05/images_mie_Mie_Aceh_37-mie-goreng.jpg"},
  { uri:"https://www.checkinjakarta.id/images/article/ARTICLE_1976_20171128093214_700_500.jpg"},
]
type Props = {};
const CARD_HEIGHT = height /8;
const CARD_WIDTH = width/1.2;
class ListAllFood extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      
    }
  }
  componentWillMount(){
    // alert(JSON.stringify(this.props.dataFromMapsHome))
  }
  goToHelperDataProfile(){
    this.props.navigation.navigate('FinishOrder')
  }
  goBackToBuyFood(){
    this.props.navigation.navigate('BuyFood')
  }
  render() {
    // alert(JSON.stringify(this.props.dataFromMapsHome()))
    return(
      <View style={styles.container}>
        <View style={{
          height:height/15,
          backgroundColor:'#ffffff',
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5,}} >
        <View style={{flexDirection:"row",flex:1}}>
          <View style={{width:40}}>
            <View style={{flex:1,alignItems:'center',justifyContent:'center',}}>                  
              <TouchableOpacity onPress={()=>{this.goBackToBuyFood()}}>
              <Image 
                source={Images.arrowBackIcon}
                style={{width:width/22,height:width/22,alignSelf:'center'}}
              />
              </TouchableOpacity>
            </View>
          </View>
            <View style={{flex:2,alignSelf:'center',justifyContent:'flex-start',}}>
              <Text style={{fontSize:width/25,fontWeight:'bold',left:'5%',color:'#454545'}}>Pedagang di dekatmu</Text>
            </View>
              <View style={{flex:1,justifyContent:'flex-end',alignItems:'flex-end'}}>
              </View>
          </View>
          </View>
        <ScrollView vertical
          style={{marginBottom:height/10}}>
          {this.props.dataFromMapsHome ? this.props.dataFromMapsHome.map((data, index) => {
            return(
              <View style={styles.containerImage} key={index}>
              {/* <Text>{data.coordinate}</Text> */}
            <ImageBackground 
              style={styles.imageBackgroundFood}
              imageStyle={{borderRadius:10}}
              source={{uri:data.desc}}
              resizeMode="cover"
              >
              <Text style={styles.titleStyle}>{data.name}</Text>
              <View style={styles.containerImage}>
                <View style={{flex:1,flexDirection:'row',position:'absolute',bottom:'5%',left:'4%'}}>
                <View style={{flexDirection:'row',flex:2,left:'4%',}}>
                  <View style={{flex:2,borderRadius:5,height:height/25,width:width/3,backgroundColor:'rgba(208, 206, 206, 0.8)',position:'relative',justifyContent:'center',}}>
                    <Text style={{textAlign:'center',color:'#454545'}}> Gold Merchant</Text>
                  </View>
                  <View style={{position:'absolute'}}>
                    <Image
                      source={Images.goldMedalIcon}
                      style={{width:width/15,height:width/11.5,alignSelf:'center',position:'absolute',bottom:-30,left:-15}}
                    />
                  </View>
                </View>
                <View style={{flexDirection:'row',flex:2,left:'10%'}}>
                    <View style={{flex:1,borderRadius:5,height:height/25,width:width/8,justifyContent:'center',backgroundColor:'rgba(208, 206, 206, 0.8)',position:'absolute'}}>
                      <Text style={{textAlign:'center'}}>50m</Text>
                    </View>
                    <Image
                      source={Images.placeholderIcon}
                      style={{width:width/17,height:width/13,alignSelf:'center',position:'absolute',bottom:10,left:-15,}}
                    />
                  </View>
                  <View style={{flex:2,paddingBottom:10,}}>
                    <View style={{backgroundColor:'#fb4a6f',height:height/25,borderRadius:5,width:width/10,justifyContent:'center',left:'46%'}}>                      
                      <Text style={{color:'white',textAlign:'center',fontSize:width/30}}>Buka</Text>
                    </View>
                  </View>
                </View>
              </View>
            </ImageBackground>
          </View>
            )
          }): null}
        </ScrollView>
        <View style={{height:height/11.5,width:width,backgroundColor:'#ffffff',bottom:0,position:'absolute'}}>
          <View style={{position:'relative',height:30,backgroundColor:'#fb4a6f',width:width/1.2,alignSelf:'center',borderRadius:5,margin:10,justifyContent:'center'}}>
            <TouchableOpacity onPress={()=>{this.goToHelperDataProfile()}}>
              <Text style={{textAlign:'center',color:'#fff'}}>Pesan</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'center'
  },
  titleStyle:{
    fontWeight: 'bold',
    color: 'white',
    // paddingLeft:10
    position: 'absolute', // child
    bottom: '30%', // position where you want
    left: '4%'
  },
  imageBackgroundFood:{
    width:width/1.1,
    height:height/4.5,
    position: 'relative',
    borderRadius:30
  },
  containerImage:{
    flex:1,
    paddingTop:height/30,
    justifyContent:'center',
    alignItems:'center',
  },
  cardImage: {
    flex: 1,
    width: width/1.3,
    height: height/8,
    alignSelf: "center",
  },
  card: {
    elevation: 2,
    backgroundColor: "#FFF",
    marginHorizontal: 10,
    shadowColor: "#000",
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { x: 2, y: -2 },
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    overflow: "hidden",
    borderRadius:5
  },
  floatingButtonCancelOrder:{
    backgroundColor:'#fb4a6f',
    borderRadius:5,
    height:height/18,
    width:width/3,
    position:'absolute',
    right:'5%',
    top:'1%',
    bottom:'4%',
    justifyContent:'center',
    alignItems:'center',
    flex:3
  }
});

const mapStateToProps = (state) => {
  return {
    dataFromMapsHome: state.filterData.dataFromMapsHome,
    merchantData: state.filterData.merchantData,
    // indicatorLogin: state.filterData.indicatorLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dataFromMaps : (value) => { dispatch (dataFromMaps(value))}
    // checkAsyncTriger: (data) => { dispatch( checkAsyncTriger(data) ) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListAllFood)
