import { combineReducers } from 'redux'

const semuaData = {
  merchantData:'',
  dataFromMapsHome: '',
  dataChoosen:'',
  merchantDataChoosen:null,
  merchantHasBeenChoose:'',
  dataFromBuyFood:[],
  dataFromChooseFood:'',
  dataSubtotalPrice:'',
  dataFetchCategorySnack:'',
  dataFetchCategoryWestern:'',
  dataFetchCategoryMakananTradisional:'',
  dataFetchCategoryFastFood:'',
  dataFetchRecommended:'',
  dataLongLong:'',
  dataPlaceOrder:'',
  dataReadyForOrder:'',
  dataHelperPage:'',
}

const filterData = ( state = semuaData, action) => {
  switch (action.type) {
    case 'PLACE_ORDER_DATA':
      return {...state,dataPlaceOrder: action.data}
    case 'SEARCH_HELPER_PAGE':
      return {...state,dataHelperPage: action.data}
    case 'READY_FOR_ORDER_DATA':
      return {...state,dataReadyForOrder: action.data}
    case 'FETCH_PRODUCT_DATA':
      return {...state,dataChoosen: action.data}
    case 'FETCH_MERCHANT_CATEGORY_SNACK':
      return {...state,dataFetchCategorySnack: action.data}
    case 'FETCH_MERCHANT_CATEGORY_WESTERN':
      return {...state,dataFetchCategoryWestern: action.data}
    case 'FETCH_MERCHANT_CATEGORY_MAKANAN_TRADISIONAL':
      return {...state,dataFetchCategoryMakananTradisional: action.data}
    case 'FETCH_MERCHANT_CATEGORY_FAST_FOOD':
      return {...state,dataFetchCategoryFastFood: action.data}
    case 'FETCH_MERCHANT_RECOMMENDED':
      return {...state,dataFetchRecommended: action.data}
    case 'SUBTOTAL_PRICE':
      return {...state,dataSubtotalPrice: action.data}
    case 'FETCH_MERCHANT_DATA':
      return {...state, merchantData:action.data}
    case 'GET_DATA_FROM_MAPS':
      return {...state, dataFromMapsHome: action.data}
    case 'MERCHANT_DATA_CHOOSEN':
      return {...state, merchantDataChoosen :action.data}
    case 'MERCHANT_HAS_BEEN_CHOOSEN':
      return {...state, merchantHasBeenChoosen :action.data}
    case 'AFTER_CHOOSE_FOOD': 
      return {...state, dataFromBuyFood:action.data}
    case 'DATA_FROM_CHOOSE_FOOD': 
      return {...state, dataFromChooseFood:action.data}
    case 'DATA_LONG': 
      return {...state, dataLongLong:action.data}
    default:
      return state
  }
}

export default combineReducers({
  filterData
})
