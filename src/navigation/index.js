import React, { Component } from 'react';
import {
  View,
  Image,
  Dimensions
} from 'react-native'
import { TabNavigator,DrawerNavigator,StackNavigator } from 'react-navigation';

import Dashboard from "../component/dashboard-user.component/dashboard.component";
import FoodOrder from '../component/food-order.component/food-order.component';
import Home from '../component/home.component/home.component';
import BuyFood from '../component/buy-food.component/buy-food.component';
import Ordered from '../component/ordered.compononet/ordered.component';
import Maps from '../component/maps-home.component/maps-home.component'
import Account from '../component/account.component/account.component';
import FoodChoose from '../component/food-choose.component/food-choose.component';
import PaymentOrdered from '../component/payment-ordered.component/payment-ordered.component';
import ListFood from '../component/buy-food.component/list-all-food.component';
import FilterFood from '../component/home.component/filter.component'
import HelperAccept from '../component/helper-profile.component/helper-accept.component/helper-accept.component';
import HelperDataProfile from '../component/helper-profile.component/helper-data-profile.component/helper-data-profile.component';
import SearchHelper from '../component/search-helper.component/search-helper.component'
import FinishOrder from '../component/finish-order.component/finish-order.component';
import CustomIcon from '../component/CustomIcon';
import MenuFood from '../component/buy-food.component/menu-food.component';
import Images from '@assets/images';
import LoginUser from '../component/dashboard-user.component/login-user.component/login-user.component'
const {height,width} = Dimensions.get('window')
export const Beranda = DrawerNavigator({
  
  Beranda: {
    screen: Home,
  },
  FoodOrder: {
    screen: FoodOrder,
  },
  BuyFood: {
    screen: BuyFood,
    navigationOptions: {
      tabBarVisible: false ,
    }
  },
  ListFood:{
    screen: ListFood,
    navigationOptions: {
      tabBarVisible:false,
      // drawerLabel:()=> null
    }
  },
  FoodChoose: {
    screen: FoodChoose,
    navigationOptions: {
      tabBarVisible: false ,
      // drawerLabel:()=> null
    }    
  },
  PaymentOrdered: {
    screen: PaymentOrdered,
    navigationOptions: {
      tabBarVisible: false ,
      // drawerLabel:()=> null
    }    
  },
  SearchHelper:{
    screen: SearchHelper,
    navigationOptions: {
      tabBarVisible: false ,
      drawerLabel:()=> null
    } 
  },
  HelperAccept: {
    screen: HelperAccept,
    navigationOptions:{
      // tabBarVisible:false,
      drawerLabel:()=> null
    }
  },
  HelperDataProfile:{
    screen: HelperDataProfile,
    navigationOptions:{
      // tabBarVisible:false,
      drawerLabel:()=> null
    }
  },
  FinishOrder: {
    screen: FinishOrder,
    navigationOptions:{
      tabBarVisible:false,
      drawerLabel:()=> null
    }
  },

  
},{
  headerMode: 'none',
  cardStyle: {
    backgroundColor: 'transparent'
  },
  contentComponent: ({ navigation, router, getLabel, renderIcon }) => {
    return(
      <View style={{flex: 1, alignItems: 'center',}}>
        <MenuFood
        goToOrderAfterChoose={() => { navigation.navigate('FoodChoose') }}
        goToListFood={() => { navigation.navigate('ListFood')}}
        />
      </View>
    )
  }
})

export const MapPeta=DrawerNavigator ({
  Maps: {
    screen: Maps,
    navigationOptions:{
      tabBarVisible:false,
      drawerLabel:()=> null
    }
  },FilterFood:{
    screen: FilterFood,
    navigationOptions:{
      tabBarVisible:false,
      drawerLabel:()=> null
    }
  },
  Dashboard:{
    screen:Dashboard,
    navigationOptions: {
      tabBarVisible: false ,
    }
  },
  LoginUser:{
    screen:LoginUser,
    navigationOptions: {
      tabBarVisible: false ,
    }
  },
})
// export const HelperPages= DrawerNavigator ({
  
  
// })
export const Tab = TabNavigator ({
  Beranda: {
    screen: Beranda,
    navigationOptions:{
      tabBarIcon:({focused,tintColor}) => 
      <Image 
        source={focused ? Images.homeOnIcon: Images.homeOffIcon}
        style={{width:width/18,height:width/18,alignSelf:'center'}}
        color={tintColor}
      />      
    }
  },
  Ordered: {
    screen: Ordered,
    navigationOptions:{
      tabBarIcon:({focused,tintColor}) => 
      <Image 
        source={focused ? Images.orderOnIcon: Images.orderOffIcon}
        style={{width:width/18,height:width/18,alignSelf:'center'}}
        color={tintColor}
      />
    }
  },
  MapPeta: {
    screen: MapPeta,
    navigationOptions:{
      tabBarIcon:({focused,tintColor}) => 
      <Image 
        source={focused ? Images.favouriteOnIcon: Images.favouriteOffIcon}
        style={{width:width/18,height:width/18,alignSelf:'center'}}
        color={tintColor}
      />
    }
  },
  Akun: {
    screen: Account,
    navigationOptions:{
      tabBarVisible:false,
      tabBarIcon:({focused,tintColor}) => 
      <Image 
        source={focused ? Images.userOnIcon: Images.userOffIcon}
        style={{width:width/18,height:width/18,alignSelf:'center'}}
        color={tintColor}
      />
    }
  }
},{
  navigationOptions: ({ navigation }) => ({
    headerTintColor: '#ffa500',
    showIcon: true,
    tabBarIcon: ({ focused, tintColor }) => {
      const { routeName } = navigation.state;
      let iconName;
      if (routeName === 'Home') {
        iconName = `ios-information-circle${focused ? '' : '-outline'}`;
      } else if (routeName === 'Settings') {
        iconName = `ios-options${focused ? '' : '-outline'}`;
      }

      // You can return any component that you like here! We usually use an
      // icon component from react-native-vector-icons
      return <Ionicons name={iconName} size={25} color={tintColor} />;
    },
  }),
  // tabBarComponent: TabBarBottom,
  tabBarPosition: 'bottom',
  tabBarOptions: {
    showIcon: true,
    showLabel: true,
    activeTintColor: '#FB4A6F',
    inactiveTintColor: '#9A8C8E',
    activeBackgroundColor: 'red',
    inactiveBackgroundColor: 'blue',
    style:{     
      
      height:height/11.5,
      backgroundColor:'#ffffff'
    },
  },
  animationEnabled: false,
  swipeEnabled: false,
})
