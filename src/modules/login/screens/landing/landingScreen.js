import React from 'react';
import type {
  Element as ReactElement,
} from 'react';
import {View, Text} from 'react-native';
import {Icon} from "@up-shared/components";

class LandingScreen extends React.PureComponent<any, any> {
  constructor(props: any) {
    super(props);
  }

  render(): ReactElement<any> {
    return (
      // <View style={{flex:1}}>
        <Icon 
          name="icon-home" 
          size={50}
          color="red"
        />
      // </View>
    );
  }
}

export default LandingScreen;
